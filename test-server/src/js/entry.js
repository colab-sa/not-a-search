
import '../less/entry.less';

import React from 'react';
import ReactDOM from 'react-dom';
import Page from './page';
import { createStore, combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import {Provider} from 'react-redux';

const combinedReducers = combineReducers({
    form: formReducer
});

const rootReducer = (state, action) => combinedReducers(state, action);

ReactDOM.render((
    <Provider store={createStore(rootReducer)}>
        <Page />
    </Provider>
), document.getElementById('entry-point'));

if (module.hot) {
    module.hot.accept();
    module.hot.dispose(function () {
        document.location.reload();
    });
}



