'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _chip = require('./chip');

var _chip2 = _interopRequireDefault(_chip);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _reactPromise = require('react-promise');

var _reactPromise2 = _interopRequireDefault(_reactPromise);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Box = function (_Component) {
    _inherits(Box, _Component);

    function Box(props) {
        _classCallCheck(this, Box);

        var _this = _possibleConstructorReturn(this, (Box.__proto__ || Object.getPrototypeOf(Box)).call(this, props));

        _this.state = {
            selectedFilter: null
        };

        _this.removeFilter = _this.removeFilter.bind(_this);
        _this.clickFilter = _this.clickFilter.bind(_this);
        _this.deselectFilter = _this.deselectFilter.bind(_this);
        return _this;
    }

    _createClass(Box, [{
        key: 'removeFilter',
        value: function removeFilter(key) {
            var onRemoveFilter = this.props.onRemoveFilter;

            onRemoveFilter(key);
        }
    }, {
        key: 'clickFilter',
        value: function clickFilter(key) {
            this.setState({
                selectedFilter: key
            });
        }
    }, {
        key: 'resetSelection',
        value: function resetSelection() {
            this.setState({
                selectedFilter: null
            });
        }
    }, {
        key: 'deselectFilter',
        value: function deselectFilter(key) {
            var selectedFilter = this.state.selectedFilter;


            if (selectedFilter === key) {
                this.resetSelection();
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var _props = this.props,
                config = _props.config,
                filters = _props.filters,
                onAddFilter = _props.onAddFilter,
                isDisabled = _props.isDisabled;
            var selectedFilter = this.state.selectedFilter;


            return _react2.default.createElement(
                'div',
                { className: 'box' },
                _lodash2.default.map(filters, function (value, key) {
                    var item = config.items.find(function (item) {
                        return item.key === key;
                    });

                    if (!item) {
                        console.error('Missing config key ' + key);
                        return null;
                    }

                    return _react2.default.createElement(_reactPromise2.default, { key: key, promise: item.getDisplay(value), then: function then(val) {
                            return _react2.default.createElement(_chip2.default, { id: key, value: value, label: item.label, display: val, onClick: _this2.clickFilter, onDeselect: _this2.deselectFilter, onRemove: _this2.removeFilter, isSelected: key === selectedFilter, config: config, onAddFilter: onAddFilter, isDisabled: isDisabled });
                        }, pendingRender: _react2.default.createElement(_chip2.default, { id: key, value: value, label: item.label, display: '...', onClick: _this2.clickFilter, onDeselect: _this2.deselectFilter, onRemove: _this2.removeFilter, isSelected: key === selectedFilter, config: config, onAddFilter: onAddFilter, isDisabled: isDisabled }) });
                })
            );
        }
    }]);

    return Box;
}(_react.Component);

Box.propTypes = {
    config: _propTypes2.default.object.isRequired,
    filters: _propTypes2.default.object.isRequired,
    onAddFilter: _propTypes2.default.func.isRequired,
    onRemoveFilter: _propTypes2.default.func.isRequired,
    isDisabled: _propTypes2.default.bool
};

exports.default = Box;