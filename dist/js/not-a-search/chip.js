'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _popup = require('./popup');

var _popup2 = _interopRequireDefault(_popup);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Chip = function (_Component) {
    _inherits(Chip, _Component);

    function Chip(props) {
        _classCallCheck(this, Chip);

        var _this = _possibleConstructorReturn(this, (Chip.__proto__ || Object.getPrototypeOf(Chip)).call(this, props));

        _this.remove = _this.remove.bind(_this);
        _this.getId = _this.getId.bind(_this);
        _this.click = _this.click.bind(_this);
        _this.deselect = _this.deselect.bind(_this);
        return _this;
    }

    _createClass(Chip, [{
        key: 'getId',
        value: function getId() {
            return this.props.id;
        }
    }, {
        key: 'remove',
        value: function remove() {
            var _props = this.props,
                onRemove = _props.onRemove,
                id = _props.id,
                isDisabled = _props.isDisabled;

            if (isDisabled) return;
            onRemove(id);
        }
    }, {
        key: 'togglePopup',
        value: function togglePopup() {
            var isDisabled = this.props.isDisabled;
            var popup = this.refs.popup;


            if (isDisabled) return;

            popup.toggle();
        }
    }, {
        key: 'click',
        value: function click(e) {
            e.preventDefault();

            var _props2 = this.props,
                id = _props2.id,
                onClick = _props2.onClick,
                isDisabled = _props2.isDisabled;
            var popup = this.refs.popup;


            if (isDisabled) return;

            onClick(id);
            popup.toggle();
        }
    }, {
        key: 'deselect',
        value: function deselect() {
            var _props3 = this.props,
                onDeselect = _props3.onDeselect,
                id = _props3.id;

            onDeselect(id);
        }
    }, {
        key: 'render',
        value: function render() {
            var _props4 = this.props,
                label = _props4.label,
                display = _props4.display,
                isSelected = _props4.isSelected,
                config = _props4.config,
                id = _props4.id,
                onAddFilter = _props4.onAddFilter,
                value = _props4.value;


            return _react2.default.createElement(
                _popup2.default,
                { ref: 'popup', config: config, onAddFilter: onAddFilter, selectedItemKey: id, initialValues: _defineProperty({}, id, value), onClose: this.deselect },
                _react2.default.createElement(
                    'div',
                    { className: (0, _classnames2.default)('chip', { selected: isSelected }), onClick: this.click },
                    _react2.default.createElement(
                        'label',
                        { className: 'title' },
                        label
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'value' },
                        display
                    ),
                    _react2.default.createElement(
                        'a',
                        { className: 'close-btn', onClick: this.remove },
                        'X'
                    )
                )
            );
        }
    }]);

    return Chip;
}(_react.Component);

Chip.propTypes = {
    id: _propTypes2.default.string.isRequired,
    label: _propTypes2.default.string.isRequired,
    value: _propTypes2.default.any.isRequired,
    display: _propTypes2.default.string.isRequired,
    onRemove: _propTypes2.default.func.isRequired,
    isSelected: _propTypes2.default.bool.isRequired,
    onClick: _propTypes2.default.func.isRequired,
    onAddFilter: _propTypes2.default.func.isRequired,
    config: _propTypes2.default.object.isRequired,
    onDeselect: _propTypes2.default.func.isRequired,
    isDisabled: _propTypes2.default.bool
};

exports.default = Chip;