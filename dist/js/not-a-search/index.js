'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.popupPages = exports.dateType = exports.pageType = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _box = require('./box');

var _box2 = _interopRequireDefault(_box);

var _popup = require('./popup');

var _popup2 = _interopRequireDefault(_popup);

var _pageType2 = require('./popup-pages/page-type');

var _pageType = _interopRequireWildcard(_pageType2);

var _dateType2 = require('./date-type');

var _dateType = _interopRequireWildcard(_dateType2);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _when = require('./popup-pages/when');

var _when2 = _interopRequireDefault(_when);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var NotASearch = function (_Component) {
    _inherits(NotASearch, _Component);

    function NotASearch(props) {
        _classCallCheck(this, NotASearch);

        var _this = _possibleConstructorReturn(this, (NotASearch.__proto__ || Object.getPrototypeOf(NotASearch)).call(this, props));

        _this.state = {
            value: props.value || {},
            isPopupActive: false
        };

        _this.togglePopup = _this.togglePopup.bind(_this);
        _this.addFilter = _this.addFilter.bind(_this);
        _this.removeFilter = _this.removeFilter.bind(_this);
        return _this;
    }

    _createClass(NotASearch, [{
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            this.setState({
                value: nextProps.value
            });
        }
    }, {
        key: 'togglePopup',
        value: function togglePopup() {
            var _refs = this.refs,
                box = _refs.box,
                popup = _refs.popup;
            var isDisabled = this.props.isDisabled;


            if (isDisabled) return;

            popup.toggle();
            box.resetSelection();
        }
    }, {
        key: 'addFilter',
        value: function addFilter(form) {
            var _props = this.props,
                onChange = _props.onChange,
                isDisabled = _props.isDisabled;
            var value = this.state.value;


            if (isDisabled) return;

            this.setState({
                value: Object.assign({}, value, form)
            }, function () {
                onChange(Object.assign({}, value, form));
            });
        }
    }, {
        key: 'removeFilter',
        value: function removeFilter(key) {
            var _this2 = this;

            var _props2 = this.props,
                onChange = _props2.onChange,
                isDisabled = _props2.isDisabled;
            var value = this.state.value;


            if (isDisabled) return;

            this.setState({
                value: _lodash2.default.omit(value, key)
            }, function () {
                onChange(_this2.state.value);
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            var _props3 = this.props,
                config = _props3.config,
                className = _props3.className,
                isDisabled = _props3.isDisabled;
            var _state = this.state,
                value = _state.value,
                isPopupActive = _state.isPopupActive;


            return _react2.default.createElement(
                'div',
                { className: (0, _classnames2.default)('not-a-search', className, { 'popup-open': isPopupActive, disabled: isDisabled }) },
                _react2.default.createElement(
                    'div',
                    { className: 'left' },
                    _react2.default.createElement(
                        _popup2.default,
                        { ref: 'popup', config: config, onAddFilter: this.addFilter, disableKeys: _lodash2.default.keys(value), onChangeIsActive: function onChangeIsActive(isActive) {
                                _this3.setState({ isPopupActive: isActive });
                            } },
                        _react2.default.createElement(
                            'a',
                            { onClick: this.togglePopup },
                            '+'
                        )
                    )
                ),
                _react2.default.createElement(_box2.default, { ref: 'box', config: config, filters: value, onRemoveFilter: this.removeFilter, onAddFilter: this.addFilter, isDisabled: isDisabled })
            );
        }
    }]);

    return NotASearch;
}(_react.Component);

NotASearch.propTypes = {
    value: _propTypes2.default.object,
    onChange: _propTypes2.default.func.isRequired,
    config: _propTypes2.default.object.isRequired,
    className: _propTypes2.default.string,
    isDisabled: _propTypes2.default.bool
};

exports.default = NotASearch;
var pageType = exports.pageType = _pageType;
var dateType = exports.dateType = _dateType;

var popupPages = exports.popupPages = {
    When: _when2.default
};