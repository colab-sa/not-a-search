'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _dateType = require('./date-type');

var dateType = _interopRequireWildcard(_dateType);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var defaultTexts = {
    today: 'Hoje',
    yesterday: 'Ontem',
    last7days: 'Últimos 7 dias',
    last30days: 'Últimos 30 dias',
    last: 'Último(a)',
    this: 'Este(a)',
    week: 'Semana',
    month: 'Mês',
    year: 'Ano'
};

var RelativeDate = function (_Component) {
    _inherits(RelativeDate, _Component);

    function RelativeDate(props) {
        _classCallCheck(this, RelativeDate);

        var _this = _possibleConstructorReturn(this, (RelativeDate.__proto__ || Object.getPrototypeOf(RelativeDate)).call(this, props));

        var value = props.value;


        _this.state = {
            value: value || null
        };

        _this.setValue = _this.setValue.bind(_this);
        _this.renderRadioBtn = _this.renderRadioBtn.bind(_this);
        return _this;
    }

    _createClass(RelativeDate, [{
        key: 'setValue',
        value: function setValue(value) {
            var onChange = this.props.onChange;


            this.setState({
                value: value
            }, function () {
                if (onChange) {
                    onChange(value);
                }
            });
        }
    }, {
        key: 'renderRadioBtn',
        value: function renderRadioBtn(dateType, label) {
            var _this2 = this;

            var value = this.state.value;


            return _react2.default.createElement(
                'a',
                { className: (0, _classnames2.default)('radio-btn', { active: value === dateType }), onClick: function onClick(e) {
                        e.preventDefault();_this2.setValue(dateType);
                    } },
                label
            );
        }
    }, {
        key: 'render',
        value: function render() {
            var texts = this.props.texts || defaultTexts;

            return _react2.default.createElement(
                'div',
                { className: 'relative-dates' },
                _react2.default.createElement(
                    'div',
                    { className: 'panel-title' },
                    '\xA0'
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'panel' },
                    texts.today && _react2.default.createElement(
                        'div',
                        { className: 'item half' },
                        this.renderRadioBtn(dateType.today, texts.today)
                    ),
                    texts.yesterday && _react2.default.createElement(
                        'div',
                        { className: 'item half' },
                        this.renderRadioBtn(dateType.yesterday, texts.yesterday)
                    ),
                    texts.last7days && _react2.default.createElement(
                        'div',
                        { className: 'item half' },
                        this.renderRadioBtn(dateType.last7days, texts.last7days)
                    ),
                    texts.last30days && _react2.default.createElement(
                        'div',
                        { className: 'item half' },
                        this.renderRadioBtn(dateType.last30days, texts.last30days)
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'panel-title' },
                    _react2.default.createElement(
                        'span',
                        null,
                        texts.last
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'panel' },
                    _react2.default.createElement(
                        'div',
                        { className: 'item third' },
                        this.renderRadioBtn(dateType.lastWeek, texts.week)
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'item third' },
                        this.renderRadioBtn(dateType.lastMonth, texts.month)
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'item third' },
                        this.renderRadioBtn(dateType.lastYear, texts.year)
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'panel-title' },
                    _react2.default.createElement(
                        'span',
                        null,
                        texts.this
                    )
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'panel' },
                    _react2.default.createElement(
                        'div',
                        { className: 'item third' },
                        this.renderRadioBtn(dateType.thisWeek, texts.week)
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'item third' },
                        this.renderRadioBtn(dateType.thisMonth, texts.month)
                    ),
                    _react2.default.createElement(
                        'div',
                        { className: 'item third' },
                        this.renderRadioBtn(dateType.thisYear, texts.year)
                    )
                )
            );
        }
    }]);

    return RelativeDate;
}(_react.Component);

RelativeDate.propTypes = {
    value: _propTypes2.default.any,
    onChange: _propTypes2.default.func,
    texts: _propTypes2.default.object
};

exports.default = RelativeDate;