'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _dateRangePicker = require('../date-range-picker');

var _dateRangePicker2 = _interopRequireDefault(_dateRangePicker);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _reactInputMask = require('react-input-mask');

var _reactInputMask2 = _interopRequireDefault(_reactInputMask);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var stateDefinitions = {
    available: {
        color: null,
        label: 'Available'
    }
};

var dateRanges = [];

var DatePicker = function (_Component) {
    _inherits(DatePicker, _Component);

    function DatePicker(props) {
        _classCallCheck(this, DatePicker);

        var _this = _possibleConstructorReturn(this, (DatePicker.__proto__ || Object.getPrototypeOf(DatePicker)).call(this, props));

        var value = _typeof(props.value) === 'object' ? props.value || null : null;

        _this.state = {
            bottomValue: value ? {
                start: value.start.format('L'),
                end: value.end.format('L')
            } : {
                start: '',
                end: ''
            },
            value: value || null,
            states: null
        };

        _this.handleSelect = _this.handleSelect.bind(_this);
        _this.startDateChange = _this.startDateChange.bind(_this);
        _this.endDateChange = _this.endDateChange.bind(_this);

        _this.startDateBlur = _this.startDateBlur.bind(_this);
        _this.endDateBlur = _this.endDateBlur.bind(_this);

        _this.startDateKeyPress = _this.startDateKeyPress.bind(_this);
        _this.endDateKeyPress = _this.endDateKeyPress.bind(_this);
        _this.notify = _this.notify.bind(_this);
        return _this;
    }

    _createClass(DatePicker, [{
        key: 'handleSelect',
        value: function handleSelect(value, states) {
            var _this2 = this;

            this.setState({
                bottomValue: {
                    start: value.start.format('L'),
                    end: value.end.format('L')
                },
                value: value,
                states: states
            }, function () {
                _this2.notify(value);
            });
        }
    }, {
        key: 'notify',
        value: function notify(value) {
            var onChange = this.props.onChange;


            if (onChange) {
                onChange(value);
            }
        }
    }, {
        key: 'startDateChange',
        value: function startDateChange(event) {
            var value = this.state.value;


            var theValue = value;

            if ((typeof theValue === 'undefined' ? 'undefined' : _typeof(theValue)) !== 'object') {
                theValue = null;
            }

            var isOne = theValue && theValue.end.diff(theValue.start) === 0;

            this.setState({
                bottomValue: {
                    start: event.target.value,
                    end: isOne ? event.target.value : value ? value.end.format('L') : ''
                }
            });
        }
    }, {
        key: 'startDateKeyPress',
        value: function startDateKeyPress(event) {
            if (event.key === 'Enter') {
                this.startDateBlur(event);
            }
        }
    }, {
        key: 'endDateChange',
        value: function endDateChange(event) {
            var bottomValue = this.state.bottomValue;


            this.setState({
                bottomValue: {
                    start: bottomValue.start,
                    end: event.target.value
                }
            });
        }
    }, {
        key: 'endDateKeyPress',
        value: function endDateKeyPress(event) {
            if (event.key === 'Enter') {
                this.endDateBlur(event);
            }
        }
    }, {
        key: 'startDateBlur',
        value: function startDateBlur(event) {
            var _this3 = this;

            //event.preventDefault();

            var value = this.state.value;

            var theValue = value;

            if ((typeof theValue === 'undefined' ? 'undefined' : _typeof(theValue)) !== 'object') {
                theValue = null;
            }

            var isOne = theValue && theValue.end.diff(theValue.start) === 0;
            var newValue = (0, _moment2.default)(event.target.value, 'L', true);

            if (!newValue.isValid()) {
                this.setState({
                    bottomValue: {
                        start: value ? value.start.format('L') : '',
                        end: value ? value.end.format('L') : ''
                    }
                });
                return;
            }

            var nextValue = _moment2.default.range(newValue, isOne ? newValue : value ? value.end : '');

            this.setState({
                bottomValue: {
                    start: newValue.format('L'),
                    end: isOne ? newValue.format('L') : value ? value.end.format('L') : ''
                },
                value: nextValue
            }, function () {
                _this3.notify(nextValue);
            });
        }
    }, {
        key: 'endDateBlur',
        value: function endDateBlur(event) {
            var _this4 = this;

            //event.preventDefault();

            var value = this.state.value;

            var newValue = (0, _moment2.default)(event.target.value, 'L', true);

            if (!newValue.isValid()) {
                this.setState({
                    bottomValue: {
                        start: value ? value.start.format('L') : '',
                        end: value ? value.end.format('L') : ''
                    }
                });
                return;
            }

            var nextValue = _moment2.default.range(value ? value.start : '', newValue);

            this.setState({
                bottomValue: {
                    start: value ? value.start.format('L') : '',
                    end: newValue.format('L')
                },
                value: nextValue
            }, function () {
                _this4.notify(nextValue);
            });
        }
    }, {
        key: 'getDateMask',
        value: function getDateMask() {

            var display = (0, _moment2.default)(new Date(2000, 11, 31)).format('L');

            return {
                mask: display.replace('2000', '9999').replace('12', '19').replace('31', '39'),
                formatChars: {
                    '1': '[0-1]',
                    '3': '[0-3]',
                    '9': '[0-9]'
                }
            };
        }
    }, {
        key: 'renderInputText',
        value: function renderInputText(value, onChange, onKeyPress, onBlur, autoFocus) {
            return _react2.default.createElement(_reactInputMask2.default, _extends({}, this.getDateMask(), { autoFocus: autoFocus, alwaysShowMask: true, className: (0, _classnames2.default)({ invalid: value !== '' && !(0, _moment2.default)(value, 'L', true).isValid() }), type: 'text', value: value, onChange: onChange, onKeyPress: onKeyPress, onBlur: onBlur }));
        }
    }, {
        key: 'render',
        value: function render() {
            var _state = this.state,
                value = _state.value,
                bottomValue = _state.bottomValue;


            var theValue = value;

            if ((typeof theValue === 'undefined' ? 'undefined' : _typeof(theValue)) !== 'object') {
                theValue = null;
            }

            var isOne = theValue && theValue.end.diff(theValue.start) === 0;

            return _react2.default.createElement(
                'div',
                { className: 'date-picker' },
                _react2.default.createElement(_dateRangePicker2.default, {
                    locale: 'pt-br',
                    firstOfWeek: 0,
                    numberOfCalendars: 1,
                    selectionType: 'range',
                    singleDateRange: true,
                    stateDefinitions: stateDefinitions,
                    dateStates: dateRanges,
                    defaultState: 'available',
                    showLegend: false,
                    value: theValue,
                    onSelect: this.handleSelect
                }),
                isOne ? _react2.default.createElement(
                    'div',
                    { className: 'display' },
                    this.renderInputText(bottomValue.start, this.startDateChange, this.startDateKeyPress, this.startDateBlur)
                ) : _react2.default.createElement(
                    'div',
                    { className: 'display' },
                    this.renderInputText(bottomValue.start, this.startDateChange, this.startDateKeyPress, this.startDateBlur, true),
                    _react2.default.createElement(
                        'span',
                        null,
                        ' - '
                    ),
                    this.renderInputText(bottomValue.end, this.endDateChange, this.endDateKeyPress, this.endDateBlur, false)
                )
            );
        }
    }]);

    return DatePicker;
}(_react.Component);

DatePicker.propTypes = {
    value: _propTypes2.default.any,
    onChange: _propTypes2.default.func
};

exports.default = DatePicker;