'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var today = exports.today = 'today';
var yesterday = exports.yesterday = 'yesterday';
var last7days = exports.last7days = 'last7days';
var last30days = exports.last30days = 'last30days';
var lastWeek = exports.lastWeek = 'lastWeek';
var lastMonth = exports.lastMonth = 'lastMonth';
var lastYear = exports.lastYear = 'lastYear';
var thisWeek = exports.thisWeek = 'thisWeek';
var thisMonth = exports.thisMonth = 'thisMonth';
var thisYear = exports.thisYear = 'thisYear';