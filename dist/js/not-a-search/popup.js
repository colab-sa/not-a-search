'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _main = require('./popup-pages/main');

var _main2 = _interopRequireDefault(_main);

var _text = require('./popup-pages/text');

var _text2 = _interopRequireDefault(_text);

var _multi = require('./popup-pages/multi');

var _multi2 = _interopRequireDefault(_multi);

var _multiTree = require('./popup-pages/multi-tree');

var _multiTree2 = _interopRequireDefault(_multiTree);

var _radio = require('./popup-pages/radio');

var _radio2 = _interopRequireDefault(_radio);

var _when = require('./popup-pages/when');

var _when2 = _interopRequireDefault(_when);

var _reactPopoverFork = require('react-popover-fork');

var _reactPopoverFork2 = _interopRequireDefault(_reactPopoverFork);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _pageType = require('./popup-pages/page-type');

var pageType = _interopRequireWildcard(_pageType);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var _pages = {};

var Popup = function (_Component) {
    _inherits(Popup, _Component);

    function Popup(props) {
        _classCallCheck(this, Popup);

        var _this = _possibleConstructorReturn(this, (Popup.__proto__ || Object.getPrototypeOf(Popup)).call(this, props));

        _this.renderMultiTree = function () {
            var selectedItem = _this.state.selectedItem;
            var _this$props = _this.props,
                selectedItemKey = _this$props.selectedItemKey,
                initialValues = _this$props.initialValues;


            return _react2.default.createElement(_multiTree2.default, _extends({}, selectedItem.config, { onSubmit: _this.addFilter, onBack: _this.resetSelectedItem, isEdit: !!selectedItemKey, initialValues: initialValues }));
        };

        _this.state = {
            isActive: false,
            selectedItem: props.config.items.find(function (item) {
                return item.key === props.selectedItemKey;
            }) || null
        };

        _this.getIsActive = _this.getIsActive.bind(_this);
        _this.open = _this.open.bind(_this);
        _this.close = _this.close.bind(_this);
        _this.toggle = _this.toggle.bind(_this);

        _this.selectItem = _this.selectItem.bind(_this);
        _this.renderMain = _this.renderMain.bind(_this);
        _this.renderText = _this.renderText.bind(_this);
        _this.renderMulti = _this.renderMulti.bind(_this);

        _this.renderSelectedItem = _this.renderSelectedItem.bind(_this);
        _this.resetSelectedItem = _this.resetSelectedItem.bind(_this);
        _this.addFilter = _this.addFilter.bind(_this);
        _this.fixInput = _this.fixInput.bind(_this);

        return _this;
    }

    _createClass(Popup, [{
        key: 'getIsActive',
        value: function getIsActive() {
            var isActive = this.state.isActive;

            return isActive;
        }
    }, {
        key: 'open',
        value: function open() {
            var _props = this.props,
                selectedItemKey = _props.selectedItemKey,
                onChangeIsActive = _props.onChangeIsActive;


            if (onChangeIsActive) {
                onChangeIsActive(true);
            }

            if (selectedItemKey) {
                this.setState({
                    isActive: true
                });
                return;
            }

            this.setState({
                isActive: true,
                selectedItem: null
            });
        }
    }, {
        key: 'close',
        value: function close() {
            var _this2 = this;

            var onChangeIsActive = this.props.onChangeIsActive;


            if (onChangeIsActive) {
                onChangeIsActive(false);
            }

            this.setState({
                isActive: false
            }, function () {
                var onClose = _this2.props.onClose;


                if (onClose) {
                    onClose();
                }
            });
        }
    }, {
        key: 'toggle',
        value: function toggle() {
            var isActive = this.state.isActive;


            if (isActive) {
                this.close();
                return;
            }

            this.open();
        }
    }, {
        key: 'selectItem',
        value: function selectItem(item) {

            this.setState({
                selectedItem: item
            });

            switch (item.type) {
                case pageType.TEXT:
                    {

                        break;
                    }
            }
        }
    }, {
        key: 'resetSelectedItem',
        value: function resetSelectedItem() {
            this.setState({
                selectedItem: null
            });
        }
    }, {
        key: 'addFilter',
        value: function addFilter(form) {
            var onAddFilter = this.props.onAddFilter;


            onAddFilter(form);

            this.close();
        }
    }, {
        key: 'renderMain',
        value: function renderMain() {
            var _props2 = this.props,
                config = _props2.config,
                disableKeys = _props2.disableKeys;
            var items = config.items;


            return _react2.default.createElement(_main2.default, { items: items, onSelect: this.selectItem, disableKeys: disableKeys });
        }
    }, {
        key: 'renderText',
        value: function renderText() {
            var selectedItem = this.state.selectedItem;
            var _props3 = this.props,
                selectedItemKey = _props3.selectedItemKey,
                initialValues = _props3.initialValues;


            if (!_pages[selectedItem.key]) {
                _pages[selectedItem.key] = (0, _text2.default)(selectedItem.key);
            }

            var Textx = _pages[selectedItem.key];

            return _react2.default.createElement(Textx, _extends({}, selectedItem.config, { onSubmit: this.addFilter, onBack: this.resetSelectedItem, isEdit: !!selectedItemKey, initialValues: initialValues }));
        }
    }, {
        key: 'renderMulti',
        value: function renderMulti() {
            var selectedItem = this.state.selectedItem;
            var _props4 = this.props,
                selectedItemKey = _props4.selectedItemKey,
                initialValues = _props4.initialValues;


            return _react2.default.createElement(_multi2.default, _extends({}, selectedItem.config, { onSubmit: this.addFilter, onBack: this.resetSelectedItem, isEdit: !!selectedItemKey, initialValues: initialValues }));
        }
    }, {
        key: 'renderRadio',
        value: function renderRadio() {
            var selectedItem = this.state.selectedItem;
            var _props5 = this.props,
                selectedItemKey = _props5.selectedItemKey,
                initialValues = _props5.initialValues;


            var parsedInitialValues = selectedItemKey ? _defineProperty({}, selectedItem.key, initialValues[selectedItem.key].toString()) : null;

            if (!_pages[selectedItem.key]) {
                _pages[selectedItem.key] = (0, _radio2.default)(selectedItem.key);
            }

            var Radiox = _pages[selectedItem.key];

            return _react2.default.createElement(Radiox, _extends({}, selectedItem.config, { onSubmit: this.addFilter, onBack: this.resetSelectedItem, isEdit: !!selectedItemKey, initialValues: parsedInitialValues }));
        }
    }, {
        key: 'fixInput',
        value: function fixInput(input) {
            if ((typeof input === 'undefined' ? 'undefined' : _typeof(input)) === 'object') {
                if (input.start && typeof input.start === 'string' && input.end && typeof input.end === 'string') {
                    return input = _moment2.default.range(Date.parse(input.start), Date.parse(input.end));
                }

                return input;
            }
        }
    }, {
        key: 'renderWhen',
        value: function renderWhen() {
            var selectedItem = this.state.selectedItem;
            var _props6 = this.props,
                selectedItemKey = _props6.selectedItemKey,
                initialValues = _props6.initialValues;


            var parsedInitialValues = selectedItemKey ? _defineProperty({}, selectedItem.key, this.fixInput(initialValues[selectedItem.key])) : null;

            if (!_pages[selectedItem.key]) {
                _pages[selectedItem.key] = (0, _when2.default)(selectedItem.key);
            }

            var Whenx = _pages[selectedItem.key];

            return _react2.default.createElement(Whenx, _extends({}, selectedItem.config, { onSubmit: this.addFilter, onBack: this.resetSelectedItem, isEdit: !!selectedItemKey, initialValues: parsedInitialValues }));
        }
    }, {
        key: 'renderSelectedItem',
        value: function renderSelectedItem() {
            var selectedItem = this.state.selectedItem;


            if (selectedItem === null) {
                return this.renderMain();
            }

            switch (selectedItem.type) {
                case pageType.TEXT:
                    {
                        return this.renderText();
                    }
                case pageType.MULTI:
                    {
                        return this.renderMulti();
                    }
                case pageType.MULTITREE:
                    {
                        return this.renderMultiTree();
                    }
                case pageType.RADIO:
                    {
                        return this.renderRadio();
                    }
                case pageType.WHEN:
                    {
                        return this.renderWhen();
                    }
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var children = this.props.children;
            var isActive = this.state.isActive;


            return _react2.default.createElement(
                _reactPopoverFork2.default,
                { isOpen: isActive, preferPlace: 'below', onOuterAction: this.close, body: _react2.default.createElement(
                        'div',
                        { className: 'advanced-filter-popup' },
                        this.renderSelectedItem()
                    ) },
                children
            );
        }
    }]);

    return Popup;
}(_react.Component);

Popup.propTypes = {
    selectedItemKey: _propTypes2.default.string,
    initialValues: _propTypes2.default.object,
    children: _propTypes2.default.object,
    config: _propTypes2.default.object.isRequired,
    onAddFilter: _propTypes2.default.func.isRequired,
    onClose: _propTypes2.default.func,
    disableKeys: _propTypes2.default.array,
    onChangeIsActive: _propTypes2.default.func
};

exports.default = Popup;