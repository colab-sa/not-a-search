'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _layout = require('./layout');

var _layout2 = _interopRequireDefault(_layout);

var _reduxForm = require('redux-form');

var _relativeDates = require('../relative-dates');

var _relativeDates2 = _interopRequireDefault(_relativeDates);

var _datePicker = require('../date-picker');

var _datePicker2 = _interopRequireDefault(_datePicker);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var When = function (_Component) {
    _inherits(When, _Component);

    function When(props) {
        _classCallCheck(this, When);

        var _this = _possibleConstructorReturn(this, (When.__proto__ || Object.getPrototypeOf(When)).call(this, props));

        var initialValues = props.initialValues,
            name = props.name;


        var value = initialValues ? initialValues[name] : null;

        _this.state = {
            page: typeof value === 'string' || !value ? 'relative' : 'static'
        };

        _this.goToPage = _this.goToPage.bind(_this);
        _this.renderRelativeDate = _this.renderRelativeDate.bind(_this);
        _this.renderRelativeForm = _this.renderRelativeForm.bind(_this);
        _this.renderStaticForm = _this.renderStaticForm.bind(_this);
        _this.renderPage = _this.renderPage.bind(_this);
        return _this;
    }

    _createClass(When, [{
        key: 'goToPage',
        value: function goToPage(page) {
            this.setState({
                page: page
            });
        }
    }, {
        key: 'renderRelativeDate',
        value: function renderRelativeDate(data) {
            var input = data.input,
                rest = _objectWithoutProperties(data, ['input']);

            return _react2.default.createElement(_relativeDates2.default, _extends({}, input, rest));
        }
    }, {
        key: 'renderDatePicker',
        value: function renderDatePicker(data) {
            var input = data.input,
                rest = _objectWithoutProperties(data, ['input']);

            return _react2.default.createElement(_datePicker2.default, _extends({}, input, rest));
        }
    }, {
        key: 'renderRelativeForm',
        value: function renderRelativeForm() {
            var _props = this.props,
                name = _props.name,
                textBtn = _props.textBtn,
                isEdit = _props.isEdit,
                onSubmit = _props.onSubmit,
                handleSubmit = _props.handleSubmit,
                pristine = _props.pristine,
                submitting = _props.submitting,
                relativeDateTexts = _props.relativeDateTexts;

            var textBtnValue = textBtn[isEdit ? 'isEdit' : 'default'];

            return _react2.default.createElement(
                'form',
                { onSubmit: handleSubmit(onSubmit) },
                _react2.default.createElement(_reduxForm.Field, { name: name, texts: relativeDateTexts, component: this.renderRelativeDate }),
                _react2.default.createElement(
                    'div',
                    null,
                    _react2.default.createElement(
                        'button',
                        { className: 'submit-btn', type: 'submit', disabled: pristine || submitting },
                        textBtnValue
                    )
                )
            );
        }
    }, {
        key: 'renderStaticForm',
        value: function renderStaticForm() {
            var _props2 = this.props,
                name = _props2.name,
                textBtn = _props2.textBtn,
                isEdit = _props2.isEdit,
                onSubmit = _props2.onSubmit,
                handleSubmit = _props2.handleSubmit,
                pristine = _props2.pristine,
                submitting = _props2.submitting,
                invalid = _props2.invalid;

            var textBtnValue = textBtn[isEdit ? 'isEdit' : 'default'];

            return _react2.default.createElement(
                'form',
                { onSubmit: handleSubmit(onSubmit) },
                _react2.default.createElement(
                    'div',
                    { className: 'picker-wrapper' },
                    _react2.default.createElement(_reduxForm.Field, { name: name, component: this.renderDatePicker })
                ),
                _react2.default.createElement(
                    'div',
                    null,
                    _react2.default.createElement(
                        'button',
                        { className: 'submit-btn', type: 'submit', disabled: pristine || submitting || invalid },
                        textBtnValue
                    )
                )
            );
        }
    }, {
        key: 'renderPage',
        value: function renderPage() {
            var page = this.state.page;


            switch (page) {
                case 'relative':
                    return this.renderRelativeForm();
                case 'static':
                    return this.renderStaticForm();
            }
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var _props3 = this.props,
                title = _props3.title,
                onBack = _props3.onBack,
                backBtn = _props3.backBtn,
                isEdit = _props3.isEdit;
            var page = this.state.page;


            return (0, _layout2.default)(title, !isEdit && onBack, backBtn, _react2.default.createElement(
                'div',
                { className: 'when-page' },
                _react2.default.createElement(
                    'nav',
                    { className: 'menu' },
                    _react2.default.createElement(
                        'a',
                        { className: (0, _classnames2.default)('btn', { active: page === 'relative' }), onClick: function onClick(e) {
                                e.preventDefault();_this2.goToPage('relative');
                            } },
                        'Relativa'
                    ),
                    _react2.default.createElement(
                        'a',
                        { className: (0, _classnames2.default)('btn', { active: page === 'static' }), onClick: function onClick(e) {
                                e.preventDefault();_this2.goToPage('static');
                            } },
                        'Espec\xEDfica'
                    )
                ),
                this.renderPage()
            ));
        }
    }]);

    return When;
}(_react.Component);

When.propTypes = {
    isEdit: _propTypes2.default.bool,
    title: _propTypes2.default.string,
    onBack: _propTypes2.default.func,
    backBtn: _propTypes2.default.object,
    label: _propTypes2.default.string,
    name: _propTypes2.default.string.isRequired,
    textBtn: _propTypes2.default.object,
    onSubmit: _propTypes2.default.func,
    handleSubmit: _propTypes2.default.func,
    pristine: _propTypes2.default.bool,
    submitting: _propTypes2.default.bool,
    initialValues: _propTypes2.default.object,
    relativeDateTexts: _propTypes2.default.object,
    invalid: _propTypes2.default.bool
};

var validate = function validate(values) {
    var errors = {};

    var key = Object.keys(values)[0];

    if (_typeof(values[key]) === 'object') {
        if (!values[key].start.isValid() || !values[key].end.isValid()) {
            errors[key] = 'Start or end date is invalid';
        }

        if (values[key].start > values[key].end) {
            errors[key] = 'Start date is greater than end date';
        }
    }

    return errors;
};

exports.default = function (key) {
    return (0, _reduxForm.reduxForm)({
        form: 'advanced-filter/popup/when/' + key,
        validate: validate
    })(When);
};