'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var MAIN = exports.MAIN = 0;
var MULTI = exports.MULTI = 1;
var RADIO = exports.RADIO = 2;
var TEXT = exports.TEXT = 3;
var WHEN = exports.WHEN = 4;
var MULTITREE = exports.MULTITREE = 5;