'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _layout = require('./layout');

var _layout2 = _interopRequireDefault(_layout);

var _reduxForm = require('redux-form');

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _reactPromise = require('react-promise');

var _reactPromise2 = _interopRequireDefault(_reactPromise);

var _latinize = require('latinize');

var _latinize2 = _interopRequireDefault(_latinize);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Radio = function (_Component) {
    _inherits(Radio, _Component);

    function Radio(props) {
        _classCallCheck(this, Radio);

        var _this = _possibleConstructorReturn(this, (Radio.__proto__ || Object.getPrototypeOf(Radio)).call(this, props));

        _this.id = '_id_' + Math.random();

        _this.state = {};

        _this.submit = _this.submit.bind(_this);
        _this.renderOptions = _this.renderOptions.bind(_this);
        return _this;
    }

    _createClass(Radio, [{
        key: 'submit',
        value: function submit(form) {
            var _props = this.props,
                onSubmit = _props.onSubmit,
                name = _props.name,
                parseResult = _props.parseResult;


            if (parseResult) {
                onSubmit(_defineProperty({}, name, parseResult(form[name])));
                return;
            }

            onSubmit(form);
        }
    }, {
        key: 'renderOptions',
        value: function renderOptions(options) {
            var _this2 = this;

            var name = this.props.name;
            //const headOption = _.head(options);

            return _react2.default.createElement(
                'div',
                null,
                _lodash2.default.sortBy(options, function (option) {
                    return (0, _latinize2.default)(option.label.toLowerCase());
                }).map(function (option) {
                    var id = _this2.id + '_' + option.value.toString();
                    return _react2.default.createElement(
                        'div',
                        { className: 'item', key: option.value },
                        _react2.default.createElement(_reduxForm.Field, { id: id, type: 'radio', component: 'input', name: name, value: option.value.toString() }),
                        _react2.default.createElement(
                            'label',
                            { htmlFor: id },
                            option.label
                        ),
                        _react2.default.createElement('div', { className: 'check' })
                    );
                })
            );
        }
    }, {
        key: 'render',
        value: function render() {
            var _props2 = this.props,
                title = _props2.title,
                onBack = _props2.onBack,
                backBtn = _props2.backBtn,
                isEdit = _props2.isEdit,
                textBtn = _props2.textBtn,
                handleSubmit = _props2.handleSubmit,
                pristine = _props2.pristine,
                submitting = _props2.submitting,
                getOptions = _props2.getOptions;

            var textBtnValue = textBtn[isEdit ? 'isEdit' : 'default'];

            return (0, _layout2.default)(title, !isEdit && onBack, backBtn, _react2.default.createElement(
                'div',
                { className: 'radio-page' },
                _react2.default.createElement(
                    'form',
                    { onSubmit: handleSubmit(this.submit) },
                    _react2.default.createElement(
                        'div',
                        { className: 'options-box' },
                        _react2.default.createElement(_reactPromise2.default, { promise: getOptions(),
                            then: this.renderOptions,
                            pendingRender: _react2.default.createElement(
                                'span',
                                null,
                                'Carregando...'
                            ) })
                    ),
                    _react2.default.createElement(
                        'div',
                        null,
                        _react2.default.createElement(
                            'button',
                            { className: 'submit-btn', type: 'submit', disabled: pristine || submitting },
                            textBtnValue
                        )
                    )
                )
            ));
        }
    }]);

    return Radio;
}(_react.Component);

Radio.propTypes = {
    isEdit: _propTypes2.default.bool,
    title: _propTypes2.default.string,
    onBack: _propTypes2.default.func,
    backBtn: _propTypes2.default.object,
    name: _propTypes2.default.string.isRequired,
    getOptions: _propTypes2.default.func.isRequired,
    textBtn: _propTypes2.default.object,
    parseResult: _propTypes2.default.func,
    onSubmit: _propTypes2.default.func,
    handleSubmit: _propTypes2.default.func,
    pristine: _propTypes2.default.bool,
    submitting: _propTypes2.default.bool
};

exports.default = function (key) {
    return (0, _reduxForm.reduxForm)({
        form: 'advanced-filter/popup/radio/' + key
    })(Radio);
};