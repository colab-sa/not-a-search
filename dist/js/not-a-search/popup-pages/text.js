'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _layout = require('./layout');

var _layout2 = _interopRequireDefault(_layout);

var _reduxForm = require('redux-form');

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Text = function (_Component) {
    _inherits(Text, _Component);

    function Text(props) {
        _classCallCheck(this, Text);

        var _this = _possibleConstructorReturn(this, (Text.__proto__ || Object.getPrototypeOf(Text)).call(this, props));

        var methods = ['submit', 'renderField'];
        _lodash2.default.forEach(methods, function (method) {
            return _this[method] = _this[method].bind(_this);
        });
        return _this;
    }

    _createClass(Text, [{
        key: 'submit',
        value: function submit(form) {
            var _props = this.props,
                onSubmit = _props.onSubmit,
                input = _props.input,
                parseResult = _props.parseResult;


            if (parseResult) {
                onSubmit(_defineProperty({}, input.name, parseResult(form[input.name])));
                return;
            }

            onSubmit(form);
        }
    }, {
        key: 'renderField',
        value: function renderField(params) {
            var input = params.input,
                className = params.className,
                type = params.type,
                _params$meta = params.meta,
                touched = _params$meta.touched,
                error = _params$meta.error,
                warning = _params$meta.warning,
                rest = _objectWithoutProperties(params, ['input', 'className', 'type', 'meta']);

            return _react2.default.createElement('input', _extends({}, input, { type: type || 'text', className: (0, _classnames2.default)(className, { error: touched && error, warning: touched && warning }) }, rest));
        }
    }, {
        key: 'render',
        value: function render() {
            var _props2 = this.props,
                title = _props2.title,
                onBack = _props2.onBack,
                backBtn = _props2.backBtn,
                isEdit = _props2.isEdit,
                label = _props2.label,
                input = _props2.input,
                textBtn = _props2.textBtn,
                handleSubmit = _props2.handleSubmit,
                pristine = _props2.pristine,
                submitting = _props2.submitting;


            var textBtnValue = textBtn[isEdit ? 'isEdit' : 'default'];

            return (0, _layout2.default)(title, !isEdit && onBack, backBtn, _react2.default.createElement(
                'form',
                { className: 'text-page', onSubmit: handleSubmit(this.submit) },
                _react2.default.createElement(
                    'div',
                    null,
                    label ? _react2.default.createElement(
                        'label',
                        null,
                        label
                    ) : null,
                    _react2.default.createElement(_reduxForm.Field, _extends({ autoFocus: true }, input, { component: this.renderField }))
                ),
                _react2.default.createElement(
                    'div',
                    null,
                    _react2.default.createElement(
                        'button',
                        { className: 'submit-btn', type: 'submit', disabled: pristine || submitting },
                        textBtnValue
                    )
                )
            ));
        }
    }]);

    return Text;
}(_react.Component);

Text.propTypes = {
    isEdit: _propTypes2.default.bool,
    title: _propTypes2.default.string,
    onBack: _propTypes2.default.func,
    label: _propTypes2.default.string,
    input: _propTypes2.default.object.isRequired,
    backBtn: _propTypes2.default.object,
    textBtn: _propTypes2.default.object,
    onSubmit: _propTypes2.default.func,
    handleSubmit: _propTypes2.default.func,
    pristine: _propTypes2.default.bool,
    submitting: _propTypes2.default.bool,
    parseResult: _propTypes2.default.func
};

exports.default = function (key) {
    return (0, _reduxForm.reduxForm)({
        form: 'advanced-filter/popup/text/' + key
    })(Text);
};