'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _layout = require('./layout');

var _layout2 = _interopRequireDefault(_layout);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _reactPromise = require('react-promise');

var _reactPromise2 = _interopRequireDefault(_reactPromise);

var _latinize = require('latinize');

var _latinize2 = _interopRequireDefault(_latinize);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Multi = function (_Component) {
    _inherits(Multi, _Component);

    function Multi(props) {
        _classCallCheck(this, Multi);

        var _this = _possibleConstructorReturn(this, (Multi.__proto__ || Object.getPrototypeOf(Multi)).call(this, props));

        _this.state = {
            filter: '',
            form: props.initialValues || _defineProperty({}, props.name, [])
        };

        _this.setFilter = _this.setFilter.bind(_this);
        _this.submit = _this.submit.bind(_this);
        _this.checkboxChange = _this.checkboxChange.bind(_this);
        _this.allSelectedChange = _this.allSelectedChange.bind(_this);
        return _this;
    }

    _createClass(Multi, [{
        key: 'submit',
        value: function submit(event) {
            var onSubmit = this.props.onSubmit;
            var form = this.state.form;


            event.preventDefault();

            onSubmit(form);
        }
    }, {
        key: 'setFilter',
        value: function setFilter(filter) {
            this.setState({
                filter: filter.target.value
            });
        }
    }, {
        key: 'checkboxChange',
        value: function checkboxChange(event) {
            var _props = this.props,
                name = _props.name,
                parseResult = _props.parseResult;

            var value = parseResult(event.target.value);

            var form = this.state.form || _defineProperty({}, name, []);

            var values = form[name];

            if (values.indexOf(value) !== -1) {
                this.setState({
                    form: _defineProperty({}, name, _lodash2.default.difference(values, [value]))
                });

                return;
            }

            this.setState({
                form: _defineProperty({}, name, [].concat(_toConsumableArray(values), [value]).sort())
            });
        }
    }, {
        key: 'allSelectedChange',
        value: function allSelectedChange() {
            var _this2 = this;

            var _props2 = this.props,
                name = _props2.name,
                getOptions = _props2.getOptions;


            var form = this.state.form || _defineProperty({}, name, []);

            getOptions().then(function (options) {

                if (form[name].length === options.length) {
                    _this2.setState({
                        form: _defineProperty({}, name, [])
                    });
                    return;
                }

                _this2.setState({
                    form: _defineProperty({}, name, options.map(function (option) {
                        return option.value;
                    }))
                });
            }).catch(function (err) {
                console.log(err);
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            var _props3 = this.props,
                title = _props3.title,
                onBack = _props3.onBack,
                name = _props3.name,
                backBtn = _props3.backBtn,
                isEdit = _props3.isEdit,
                textBtn = _props3.textBtn,
                getOptions = _props3.getOptions,
                selectAll = _props3.selectAll,
                filterFieldPlaceholder = _props3.filterFieldPlaceholder,
                filterIcon = _props3.filterIcon;
            var _state = this.state,
                filter = _state.filter,
                form = _state.form;


            var adjustedFilter = (0, _latinize2.default)(filter.toLowerCase());
            var textBtnValue = textBtn[isEdit ? 'isEdit' : 'default'];

            return (0, _layout2.default)(title, !isEdit && onBack, backBtn, _react2.default.createElement(
                'div',
                { className: 'multi-page' },
                _react2.default.createElement(
                    'div',
                    { className: 'filter-wrapper' },
                    _react2.default.createElement('input', { autoFocus: true, className: 'filter-field', type: 'text', placeholder: filterFieldPlaceholder, value: filter, onChange: this.setFilter }),
                    filterIcon
                ),
                _react2.default.createElement(
                    'form',
                    { onSubmit: this.submit },
                    _react2.default.createElement(
                        'div',
                        { className: 'options-box' },
                        _react2.default.createElement(_reactPromise2.default, { promise: getOptions(), then: function then(options) {
                                return _react2.default.createElement(
                                    'div',
                                    null,
                                    selectAll && !adjustedFilter ? _react2.default.createElement(
                                        'label',
                                        null,
                                        _react2.default.createElement('input', { type: 'checkbox', checked: options.length === form[name].length, onChange: _this3.allSelectedChange }),
                                        ' Todos / Nenhum'
                                    ) : null,
                                    !adjustedFilter && _react2.default.createElement('hr', null),
                                    options.filter(function (option) {
                                        return (0, _latinize2.default)(option.label.toLowerCase()).indexOf(adjustedFilter) !== -1;
                                    }).map(function (option) {
                                        return _react2.default.createElement(
                                            'label',
                                            { key: option.value },
                                            _react2.default.createElement('input', { type: 'checkbox', name: name, value: option.value, checked: form[name].indexOf(option.value) !== -1, onChange: _this3.checkboxChange }),
                                            option.label
                                        );
                                    })
                                );
                            }, pendingRender: _react2.default.createElement(
                                'span',
                                null,
                                'Carregando...'
                            ) })
                    ),
                    _react2.default.createElement(
                        'div',
                        null,
                        _react2.default.createElement(
                            'button',
                            { className: 'submit-btn', type: 'submit', disabled: form[name].length === 0 },
                            textBtnValue
                        )
                    )
                )
            ));
        }
    }]);

    return Multi;
}(_react.Component);

Multi.propTypes = {
    isEdit: _propTypes2.default.bool,
    title: _propTypes2.default.string,
    onBack: _propTypes2.default.func,
    backBtn: _propTypes2.default.object,
    name: _propTypes2.default.string.isRequired,
    getOptions: _propTypes2.default.func.isRequired,
    textBtn: _propTypes2.default.object,
    parseResult: _propTypes2.default.func,
    onSubmit: _propTypes2.default.func,
    initialValues: _propTypes2.default.object,
    selectAll: _propTypes2.default.string,
    filterFieldPlaceholder: _propTypes2.default.string,
    filterIcon: _propTypes2.default.object
};

exports.default = Multi;