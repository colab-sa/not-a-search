'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (title, onBack, backBtn, content) {
    return _react2.default.createElement(
        'div',
        { className: 'popup-page' },
        title ? _react2.default.createElement(
            'div',
            { className: 'header' },
            onBack ? _react2.default.createElement(
                'a',
                { className: 'back-btn', onClick: function onClick(e) {
                        e.preventDefault();
                        onBack();
                    } },
                backBtn
            ) : null,
            _react2.default.createElement(
                'span',
                { className: 'title' },
                title
            )
        ) : null,
        _react2.default.createElement(
            'div',
            { className: 'content' },
            content
        )
    );
};