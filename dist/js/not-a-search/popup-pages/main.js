'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _layout = require('./layout');

var _layout2 = _interopRequireDefault(_layout);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Main = function (_Component) {
    _inherits(Main, _Component);

    function Main(props) {
        _classCallCheck(this, Main);

        var _this = _possibleConstructorReturn(this, (Main.__proto__ || Object.getPrototypeOf(Main)).call(this, props));

        _this.seletedItem = null;
        _this.select = _this.select.bind(_this);
        return _this;
    }

    _createClass(Main, [{
        key: 'select',
        value: function select(item) {
            var onSelect = this.props.onSelect;


            onSelect(item);
        }
    }, {
        key: 'mainRender',
        value: function mainRender() {}
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var _props = this.props,
                items = _props.items,
                disableKeys = _props.disableKeys;


            return (0, _layout2.default)(null, null, null, _react2.default.createElement(
                'div',
                { className: 'main-popup-page' },
                _react2.default.createElement(
                    'ul',
                    null,
                    (0, _lodash2.default)(items).filter(function (item) {
                        return !item.hidden;
                    }).map(function (item) {
                        return _react2.default.createElement(
                            'li',
                            { key: Math.random() },
                            _react2.default.createElement(
                                'a',
                                { className: (0, _classnames2.default)({ disabled: disableKeys && _lodash2.default.includes(disableKeys || [], item.key) }), onClick: function onClick(e) {
                                        e.preventDefault();_this2.select(item);
                                    } },
                                item.label
                            )
                        );
                    }).value()
                )
            ));
        }
    }]);

    return Main;
}(_react.Component);

Main.propTypes = {
    items: _propTypes2.default.array.isRequired,
    disableKeys: _propTypes2.default.array,
    onSelect: _propTypes2.default.func.isRequired
};

exports.default = Main;