'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _shallowEqual = require('./utils/shallowEqual');

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _bemCx = require('./utils/bemCx');

var _bemCx2 = _interopRequireDefault(_bemCx);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var PaginationArrow = function (_Component) {
  _inherits(PaginationArrow, _Component);

  function PaginationArrow(props) {
    _classCallCheck(this, PaginationArrow);

    var _this = _possibleConstructorReturn(this, (PaginationArrow.__proto__ || Object.getPrototypeOf(PaginationArrow)).call(this, props));

    _this.render = _this.render.bind(_this);
    _this.shouldComponentUpdate = _this.shouldComponentUpdate.bind(_this);
    _this.getChildContext = _this.getChildContext.bind(_this);
    _this.getBemNamespace = _this.getBemNamespace.bind(_this);
    _this.getBemBlock = _this.getBemBlock.bind(_this);
    _this.cx = _this.cx.bind(_this);
    return _this;
  }

  _createClass(PaginationArrow, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          disabled = _props.disabled,
          direction = _props.direction,
          onTrigger = _props.onTrigger,
          props = _objectWithoutProperties(_props, ['disabled', 'direction', 'onTrigger']);

      var modifiers = _defineProperty({}, direction, true);
      var states = { disabled: disabled };

      var elementOpts = {
        modifiers: modifiers,
        states: states
      };

      var iconOpts = {
        element: 'PaginationArrowIcon',
        modifiers: modifiers,
        states: states
      };

      return _react2.default.createElement(
        'div',
        _extends({ className: this.cx(elementOpts) }, props, { onClick: onTrigger }),
        _react2.default.createElement('div', { className: this.cx(iconOpts) })
      );
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      return !(0, _shallowEqual2.default)(this.props, nextProps) || !(0, _shallowEqual2.default)(this.state, nextState);
    }
  }, {
    key: 'getChildContext',
    value: function getChildContext() {
      return {
        bemNamespace: this.getBemNamespace(),
        bemBlock: this.getBemBlock()
      };
    }
  }, {
    key: 'getBemNamespace',
    value: function getBemNamespace() {
      if (this.props.bemNamespace) {
        return this.props.bemNamespace;
      }
      if (this.context.bemNamespace) {
        return this.context.bemNamespace;
      }
      return null;
    }
  }, {
    key: 'getBemBlock',
    value: function getBemBlock() {
      if (this.props.bemBlock) {
        return this.props.bemBlock;
      }
      if (this.context.bemBlock) {
        return this.context.bemBlock;
      }
      return null;
    }
  }, {
    key: 'cx',
    value: function cx() {
      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var opts = {
        namespace: this.getBemNamespace(),
        element: 'PaginationArrow',
        block: this.getBemBlock()
      };

      Object.assign(opts, options);
      return (0, _bemCx2.default)(opts);
    }
  }]);

  return PaginationArrow;
}(_react.Component);

PaginationArrow.propTypes = {
  disabled: _propTypes2.default.bool,
  onTrigger: _propTypes2.default.func,
  direction: _propTypes2.default.oneOf(['next', 'previous']),
  bemNamespace: _propTypes2.default.string,
  bemBlock: _propTypes2.default.string
};
PaginationArrow.contextTypes = {
  bemNamespace: _propTypes2.default.string,
  bemBlock: _propTypes2.default.string
};
PaginationArrow.childContextTypes = {
  bemNamespace: _propTypes2.default.string,
  bemBlock: _propTypes2.default.string
};
PaginationArrow.defaultProps = {
  disabled: false
};
exports.default = PaginationArrow;