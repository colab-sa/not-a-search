'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

require('moment-range');

var _calendar = require('calendar');

var _calendar2 = _interopRequireDefault(_calendar);

var _immutable = require('immutable');

var _immutable2 = _interopRequireDefault(_immutable);

var _CustomPropTypes = require('../utils/CustomPropTypes');

var _CustomPropTypes2 = _interopRequireDefault(_CustomPropTypes);

var _isMomentRange = require('../utils/isMomentRange');

var _isMomentRange2 = _interopRequireDefault(_isMomentRange);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _shallowEqual = require('../utils/shallowEqual');

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _bemCx = require('../utils/bemCx');

var _bemCx2 = _interopRequireDefault(_bemCx);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var lang = (0, _moment2.default)().localeData();

var WEEKDAYS = _immutable2.default.List(lang._weekdays).zip(_immutable2.default.List(lang._weekdaysShort));
var MONTHS = _immutable2.default.List(lang._months);

var CalendarMonth = function (_Component) {
  _inherits(CalendarMonth, _Component);

  function CalendarMonth(props) {
    _classCallCheck(this, CalendarMonth);

    var _this = _possibleConstructorReturn(this, (CalendarMonth.__proto__ || Object.getPrototypeOf(CalendarMonth)).call(this, props));

    _this.renderDay = _this.renderDay.bind(_this);
    _this.renderWeek = _this.renderWeek.bind(_this);
    _this.renderDayHeaders = _this.renderDayHeaders.bind(_this);
    _this.handleYearChange = _this.handleYearChange.bind(_this);
    _this.renderYearChoice = _this.renderYearChoice.bind(_this);
    _this.renderHeaderYear = _this.renderHeaderYear.bind(_this);
    _this.handleMonthChange = _this.handleMonthChange.bind(_this);
    _this.renderMonthChoice = _this.renderMonthChoice.bind(_this);
    _this.renderHeaderMonth = _this.renderHeaderMonth.bind(_this);
    _this.renderHeader = _this.renderHeader.bind(_this);

    _this.render = _this.render.bind(_this);
    _this.shouldComponentUpdate = _this.shouldComponentUpdate.bind(_this);
    _this.getChildContext = _this.getChildContext.bind(_this);
    _this.getBemNamespace = _this.getBemNamespace.bind(_this);
    _this.getBemBlock = _this.getBemBlock.bind(_this);
    _this.cx = _this.cx.bind(_this);
    return _this;
  }

  _createClass(CalendarMonth, [{
    key: 'renderDay',
    value: function renderDay(date, i) {
      var _props = this.props,
          CalendarDate = _props.dateComponent,
          value = _props.value,
          highlightedDate = _props.highlightedDate,
          highlightedRange = _props.highlightedRange,
          hideSelection = _props.hideSelection,
          enabledRange = _props.enabledRange,
          props = _objectWithoutProperties(_props, ['dateComponent', 'value', 'highlightedDate', 'highlightedRange', 'hideSelection', 'enabledRange']);

      var d = (0, _moment2.default)(date);

      var isInSelectedRange = void 0;
      var isSelectedDate = void 0;
      var isSelectedRangeStart = void 0;
      var isSelectedRangeEnd = void 0;

      if (!hideSelection && value && _moment2.default.isMoment(value) && value.isSame(d, 'day')) {
        isSelectedDate = true;
      } else if (!hideSelection && value && (0, _isMomentRange2.default)(value) && value.contains(d)) {
        isInSelectedRange = true;

        isSelectedRangeStart = value.start.isSame(d, 'day');
        isSelectedRangeEnd = value.end.isSame(d, 'day');
      }

      return _react2.default.createElement(CalendarDate, _extends({
        key: i,
        isToday: d.isSame((0, _moment2.default)(), 'day'),
        isDisabled: !enabledRange.contains(d),
        isHighlightedDate: !!(highlightedDate && highlightedDate.isSame(d, 'day')),
        isHighlightedRangeStart: !!(highlightedRange && highlightedRange.start.isSame(d, 'day')),
        isHighlightedRangeEnd: !!(highlightedRange && highlightedRange.end.isSame(d, 'day')),
        isInHighlightedRange: !!(highlightedRange && highlightedRange.contains(d)),
        isSelectedDate: isSelectedDate,
        isSelectedRangeStart: isSelectedRangeStart,
        isSelectedRangeEnd: isSelectedRangeEnd,
        isInSelectedRange: isInSelectedRange,
        date: d
      }, props));
    }
  }, {
    key: 'renderWeek',
    value: function renderWeek(dates, i) {
      var days = dates.map(this.renderDay);
      return _react2.default.createElement(
        'tr',
        { className: this.cx({ element: 'Week' }), key: i },
        days.toJS()
      );
    }
  }, {
    key: 'renderDayHeaders',
    value: function renderDayHeaders() {
      var firstOfWeek = this.props.firstOfWeek;

      var indices = _immutable2.default.Range(firstOfWeek, 7).concat(_immutable2.default.Range(0, firstOfWeek));

      var headers = indices.map(function (index) {
        var lang = (0, _moment2.default)().localeData();
        var WEEKDAYS = _immutable2.default.List(lang._weekdays).zip(_immutable2.default.List(lang._weekdaysShort));

        var weekday = WEEKDAYS.get(index);
        return _react2.default.createElement(
          'th',
          { className: this.cx({ element: 'WeekdayHeading' }), key: weekday, scope: 'col' },
          _react2.default.createElement(
            'abbr',
            { title: weekday[0] },
            weekday[1]
          )
        );
      }.bind(this));

      return _react2.default.createElement(
        'tr',
        { className: this.cx({ element: 'Weekdays' }) },
        headers.toJS()
      );
    }
  }, {
    key: 'handleYearChange',
    value: function handleYearChange(event) {
      this.props.onYearChange(parseInt(event.target.value, 10));
    }
  }, {
    key: 'renderYearChoice',
    value: function renderYearChoice(year) {
      var enabledRange = this.props.enabledRange;


      if (year < enabledRange.start.year()) {
        return null;
      }

      if (year > enabledRange.end.year()) {
        return null;
      }

      return _react2.default.createElement(
        'option',
        { key: year, value: year },
        year
      );
    }
  }, {
    key: 'renderHeaderYear',
    value: function renderHeaderYear() {
      var firstOfMonth = this.props.firstOfMonth;

      var y = firstOfMonth.year();
      var years = _immutable2.default.Range(y - 5, y).concat(_immutable2.default.Range(y, y + 10));
      var choices = years.map(this.renderYearChoice);
      var modifiers = { year: true };
      return _react2.default.createElement(
        'span',
        { className: this.cx({ element: 'MonthHeaderLabel', modifiers: modifiers }) },
        firstOfMonth.format('YYYY'),
        this.props.disableNavigation ? null : _react2.default.createElement(
          'select',
          { className: this.cx({ element: 'MonthHeaderSelect' }), value: y, onChange: this.handleYearChange },
          choices.toJS()
        )
      );
    }
  }, {
    key: 'handleMonthChange',
    value: function handleMonthChange(event) {
      this.props.onMonthChange(parseInt(event.target.value, 10));
    }
  }, {
    key: 'renderMonthChoice',
    value: function renderMonthChoice(month, i) {
      var _props2 = this.props,
          firstOfMonth = _props2.firstOfMonth,
          enabledRange = _props2.enabledRange;

      var disabled = false;
      var year = firstOfMonth.year();

      if ((0, _moment2.default)({ years: year, months: i + 1, date: 1 }).unix() < enabledRange.start.unix()) {
        disabled = true;
      }

      if ((0, _moment2.default)({ years: year, months: i, date: 1 }).unix() > enabledRange.end.unix()) {
        disabled = true;
      }

      return _react2.default.createElement(
        'option',
        { key: month, value: i, disabled: disabled ? 'disabled' : null },
        month
      );
    }
  }, {
    key: 'renderHeaderMonth',
    value: function renderHeaderMonth() {
      var lang = (0, _moment2.default)().localeData();
      var MONTHS = _immutable2.default.List(lang._months);

      var firstOfMonth = this.props.firstOfMonth;

      var choices = MONTHS.map(this.renderMonthChoice);
      var modifiers = { month: true };

      return _react2.default.createElement(
        'span',
        { className: this.cx({ element: 'MonthHeaderLabel', modifiers: modifiers }) },
        firstOfMonth.format('MMMM'),
        this.props.disableNavigation ? null : _react2.default.createElement(
          'select',
          { className: this.cx({ element: 'MonthHeaderSelect' }), value: firstOfMonth.month(), onChange: this.handleMonthChange },
          choices.toJS()
        )
      );
    }
  }, {
    key: 'renderHeader',
    value: function renderHeader() {
      return _react2.default.createElement(
        'div',
        { className: this.cx({ element: 'MonthHeader' }) },
        this.renderHeaderMonth(),
        ' ',
        this.renderHeaderYear()
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _props3 = this.props,
          firstOfWeek = _props3.firstOfWeek,
          firstOfMonth = _props3.firstOfMonth;


      var cal = new _calendar2.default.Calendar(firstOfWeek);
      var monthDates = _immutable2.default.fromJS(cal.monthDates(firstOfMonth.year(), firstOfMonth.month()));
      var weeks = monthDates.map(this.renderWeek);

      return _react2.default.createElement(
        'div',
        { className: this.cx({ element: 'Month' }) },
        this.renderHeader(),
        _react2.default.createElement(
          'table',
          { className: this.cx({ element: 'MonthDates' }) },
          _react2.default.createElement(
            'thead',
            null,
            this.renderDayHeaders()
          ),
          _react2.default.createElement(
            'tbody',
            null,
            weeks.toJS()
          )
        )
      );
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      return !(0, _shallowEqual2.default)(this.props, nextProps) || !(0, _shallowEqual2.default)(this.state, nextState);
    }
  }, {
    key: 'getChildContext',
    value: function getChildContext() {
      return {
        bemNamespace: this.getBemNamespace(),
        bemBlock: this.getBemBlock()
      };
    }
  }, {
    key: 'getBemNamespace',
    value: function getBemNamespace() {
      if (this.props.bemNamespace) {
        return this.props.bemNamespace;
      }
      if (this.context.bemNamespace) {
        return this.context.bemNamespace;
      }
      return null;
    }
  }, {
    key: 'getBemBlock',
    value: function getBemBlock() {
      if (this.props.bemBlock) {
        return this.props.bemBlock;
      }
      if (this.context.bemBlock) {
        return this.context.bemBlock;
      }
      return null;
    }
  }, {
    key: 'cx',
    value: function cx() {
      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var opts = {
        namespace: this.getBemNamespace(),
        element: 'CalendarMonth',
        block: this.getBemBlock()
      };

      Object.assign(opts, options);
      return (0, _bemCx2.default)(opts);
    }
  }]);

  return CalendarMonth;
}(_react.Component);

CalendarMonth.propTypes = {
  dateComponent: _propTypes2.default.func,
  disableNavigation: _propTypes2.default.bool,
  enabledRange: _CustomPropTypes2.default.momentRange,
  firstOfMonth: _CustomPropTypes2.default.moment,
  firstOfWeek: _propTypes2.default.oneOf([0, 1, 2, 3, 4, 5, 6]),
  hideSelection: _propTypes2.default.bool,
  highlightedDate: _propTypes2.default.object,
  highlightedRange: _propTypes2.default.object,
  onMonthChange: _propTypes2.default.func,
  onYearChange: _propTypes2.default.func,
  value: _CustomPropTypes2.default.momentOrMomentRange,
  bemNamespace: _propTypes2.default.string,
  bemBlock: _propTypes2.default.string
};
CalendarMonth.contextTypes = {
  bemNamespace: _propTypes2.default.string,
  bemBlock: _propTypes2.default.string
};
CalendarMonth.childContextTypes = {
  bemNamespace: _propTypes2.default.string,
  bemBlock: _propTypes2.default.string
};
exports.default = CalendarMonth;