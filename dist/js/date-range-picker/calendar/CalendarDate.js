'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _immutable = require('immutable');

var _immutable2 = _interopRequireDefault(_immutable);

var _CustomPropTypes = require('../utils/CustomPropTypes');

var _CustomPropTypes2 = _interopRequireDefault(_CustomPropTypes);

var _lightenDarkenColor = require('../utils/lightenDarkenColor');

var _lightenDarkenColor2 = _interopRequireDefault(_lightenDarkenColor);

var _CalendarDatePeriod = require('./CalendarDatePeriod');

var _CalendarDatePeriod2 = _interopRequireDefault(_CalendarDatePeriod);

var _CalendarHighlight = require('./CalendarHighlight');

var _CalendarHighlight2 = _interopRequireDefault(_CalendarHighlight);

var _CalendarSelection = require('./CalendarSelection');

var _CalendarSelection2 = _interopRequireDefault(_CalendarSelection);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _shallowEqual = require('../utils/shallowEqual');

var _shallowEqual2 = _interopRequireDefault(_shallowEqual);

var _bemCx = require('../utils/bemCx');

var _bemCx2 = _interopRequireDefault(_bemCx);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CalendarDate = function (_Component) {
  _inherits(CalendarDate, _Component);

  function CalendarDate(props) {
    _classCallCheck(this, CalendarDate);

    var _this = _possibleConstructorReturn(this, (CalendarDate.__proto__ || Object.getPrototypeOf(CalendarDate)).call(this, props));

    _this.state = {
      mouseDown: false
    };

    _this.mouseUp = _this.mouseUp.bind(_this);
    _this.mouseDown = _this.mouseDown.bind(_this);
    _this.touchEnd = _this.touchEnd.bind(_this);
    _this.touchStart = _this.touchStart.bind(_this);
    _this.mouseEnter = _this.mouseEnter.bind(_this);
    _this.mouseLeave = _this.mouseLeave.bind(_this);
    _this.getBemModifiers = _this.getBemModifiers.bind(_this);
    _this.getBemStates = _this.getBemStates.bind(_this);
    _this.render = _this.render.bind(_this);
    _this.shouldComponentUpdate = _this.shouldComponentUpdate.bind(_this);
    _this.getChildContext = _this.getChildContext.bind(_this);
    _this.getBemNamespace = _this.getBemNamespace.bind(_this);
    _this.getBemBlock = _this.getBemBlock.bind(_this);
    _this.cx = _this.cx.bind(_this);
    return _this;
  }

  _createClass(CalendarDate, [{
    key: 'mouseUp',
    value: function mouseUp() {
      this.props.onSelectDate(this.props.date);

      if (this.state.mouseDown) {
        this.setState({
          mouseDown: false
        });
      }
      document.removeEventListener('mouseup', this.mouseUp);
    }
  }, {
    key: 'mouseDown',
    value: function mouseDown() {
      this.setState({
        mouseDown: true
      });
      document.addEventListener('mouseup', this.mouseUp);
    }
  }, {
    key: 'touchEnd',
    value: function touchEnd() {
      this.props.onHighlightDate(this.props.date);
      this.props.onSelectDate(this.props.date);

      if (this.state.mouseDown) {
        this.setState({
          mouseDown: false
        });
      }
      document.removeEventListener('touchend', this.touchEnd);
    }
  }, {
    key: 'touchStart',
    value: function touchStart(event) {
      event.preventDefault();
      this.setState({
        mouseDown: true
      });
      document.addEventListener('touchend', this.touchEnd);
    }
  }, {
    key: 'mouseEnter',
    value: function mouseEnter() {
      this.props.onHighlightDate(this.props.date);
    }
  }, {
    key: 'mouseLeave',
    value: function mouseLeave() {
      if (this.state.mouseDown) {
        this.props.onSelectDate(this.props.date);

        this.setState({
          mouseDown: false
        });
      }
      this.props.onUnHighlightDate(this.props.date);
    }
  }, {
    key: 'getBemModifiers',
    value: function getBemModifiers() {
      var _props = this.props,
          date = _props.date,
          firstOfMonth = _props.firstOfMonth,
          today = _props.isToday;


      var otherMonth = false;
      var weekend = false;

      if (date.month() !== firstOfMonth.month()) {
        otherMonth = true;
      }

      if (date.day() === 0 || date.day() === 6) {
        weekend = true;
      }

      return { today: today, weekend: weekend, otherMonth: otherMonth };
    }
  }, {
    key: 'getBemStates',
    value: function getBemStates() {
      var _props2 = this.props,
          isSelectedDate = _props2.isSelectedDate,
          isInSelectedRange = _props2.isInSelectedRange,
          isInHighlightedRange = _props2.isInHighlightedRange,
          highlighted = _props2.isHighlightedDate,
          disabled = _props2.isDisabled;


      var selected = isSelectedDate || isInSelectedRange || isInHighlightedRange;

      return { disabled: disabled, highlighted: highlighted, selected: selected };
    }
  }, {
    key: 'render',
    value: function render() {
      var _props3 = this.props,
          date = _props3.date,
          dateRangesForDate = _props3.dateRangesForDate,
          isSelectedDate = _props3.isSelectedDate,
          isSelectedRangeStart = _props3.isSelectedRangeStart,
          isSelectedRangeEnd = _props3.isSelectedRangeEnd,
          isInSelectedRange = _props3.isInSelectedRange,
          isHighlightedDate = _props3.isHighlightedDate,
          isHighlightedRangeStart = _props3.isHighlightedRangeStart,
          isHighlightedRangeEnd = _props3.isHighlightedRangeEnd,
          isInHighlightedRange = _props3.isInHighlightedRange;


      var bemModifiers = this.getBemModifiers();
      var bemStates = this.getBemStates();
      var pending = isInHighlightedRange;

      var color = void 0;
      var amColor = void 0;
      var pmColor = void 0;
      var states = dateRangesForDate(date);
      var numStates = states.count();
      var cellStyle = {};
      var style = {};

      var highlightModifier = void 0;
      var selectionModifier = void 0;

      if (isSelectedDate || isSelectedRangeStart && isSelectedRangeEnd || isHighlightedRangeStart && isHighlightedRangeEnd) {
        selectionModifier = 'single';
      } else if (isSelectedRangeStart || isHighlightedRangeStart) {
        selectionModifier = 'start';
      } else if (isSelectedRangeEnd || isHighlightedRangeEnd) {
        selectionModifier = 'end';
      } else if (isInSelectedRange || isInHighlightedRange) {
        selectionModifier = 'segment';
      }

      if (isHighlightedDate) {
        highlightModifier = 'single';
      }

      if (numStates === 1) {
        // If there's only one state, it means we're not at a boundary
        color = states.getIn([0, 'color']);

        if (color) {

          style = {
            backgroundColor: color
          };
          cellStyle = {
            borderLeftColor: (0, _lightenDarkenColor2.default)(color, -10),
            borderRightColor: (0, _lightenDarkenColor2.default)(color, -10)
          };
        }
      } else {
        amColor = states.getIn([0, 'color']);
        pmColor = states.getIn([1, 'color']);

        if (amColor) {
          cellStyle.borderLeftColor = (0, _lightenDarkenColor2.default)(amColor, -10);
        }

        if (pmColor) {
          cellStyle.borderRightColor = (0, _lightenDarkenColor2.default)(pmColor, -10);
        }
      }

      return _react2.default.createElement(
        'td',
        { className: this.cx({ element: 'Date', modifiers: bemModifiers, states: bemStates }),
          style: cellStyle,
          onTouchStart: this.touchStart,
          onMouseEnter: this.mouseEnter,
          onMouseLeave: this.mouseLeave,
          onMouseDown: this.mouseDown },
        numStates > 1 && _react2.default.createElement(
          'div',
          { className: this.cx({ element: 'HalfDateStates' }) },
          _react2.default.createElement(_CalendarDatePeriod2.default, { period: 'am', color: amColor }),
          _react2.default.createElement(_CalendarDatePeriod2.default, { period: 'pm', color: pmColor })
        ),
        numStates === 1 && _react2.default.createElement('div', { className: this.cx({ element: 'FullDateStates' }), style: style }),
        _react2.default.createElement(
          'span',
          { className: this.cx({ element: 'DateLabel' }) },
          date.format('D')
        ),
        selectionModifier ? _react2.default.createElement(_CalendarSelection2.default, { modifier: selectionModifier, pending: pending }) : null,
        highlightModifier ? _react2.default.createElement(_CalendarHighlight2.default, { modifier: highlightModifier }) : null
      );
    }
  }, {
    key: 'shouldComponentUpdate',
    value: function shouldComponentUpdate(nextProps, nextState) {
      return !(0, _shallowEqual2.default)(this.props, nextProps) || !(0, _shallowEqual2.default)(this.state, nextState);
    }
  }, {
    key: 'getChildContext',
    value: function getChildContext() {
      return {
        bemNamespace: this.getBemNamespace(),
        bemBlock: this.getBemBlock()
      };
    }
  }, {
    key: 'getBemNamespace',
    value: function getBemNamespace() {
      if (this.props.bemNamespace) {
        return this.props.bemNamespace;
      }
      if (this.context.bemNamespace) {
        return this.context.bemNamespace;
      }
      return null;
    }
  }, {
    key: 'getBemBlock',
    value: function getBemBlock() {
      if (this.props.bemBlock) {
        return this.props.bemBlock;
      }
      if (this.context.bemBlock) {
        return this.context.bemBlock;
      }
      return null;
    }
  }, {
    key: 'cx',
    value: function cx() {
      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var opts = {
        namespace: this.getBemNamespace(),
        element: 'CalendarDate',
        block: this.getBemBlock()
      };

      Object.assign(opts, options);
      return (0, _bemCx2.default)(opts);
    }
  }]);

  return CalendarDate;
}(_react.Component);

CalendarDate.propTypes = {
  date: _CustomPropTypes2.default.moment,

  firstOfMonth: _propTypes2.default.object.isRequired,

  isSelectedDate: _propTypes2.default.bool,
  isSelectedRangeStart: _propTypes2.default.bool,
  isSelectedRangeEnd: _propTypes2.default.bool,
  isInSelectedRange: _propTypes2.default.bool,

  isHighlightedDate: _propTypes2.default.bool,
  isHighlightedRangeStart: _propTypes2.default.bool,
  isHighlightedRangeEnd: _propTypes2.default.bool,
  isInHighlightedRange: _propTypes2.default.bool,

  highlightedDate: _propTypes2.default.object,
  dateStates: _propTypes2.default.instanceOf(_immutable2.default.List),
  isDisabled: _propTypes2.default.bool,
  isToday: _propTypes2.default.bool,

  dateRangesForDate: _propTypes2.default.func,
  onHighlightDate: _propTypes2.default.func,
  onUnHighlightDate: _propTypes2.default.func,
  onSelectDate: _propTypes2.default.func,
  bemNamespace: _propTypes2.default.string,
  bemBlock: _propTypes2.default.string
};
CalendarDate.contextTypes = {
  bemNamespace: _propTypes2.default.string,
  bemBlock: _propTypes2.default.string
};
CalendarDate.childContextTypes = {
  bemNamespace: _propTypes2.default.string,
  bemBlock: _propTypes2.default.string
};
exports.default = CalendarDate;