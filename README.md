#not-a-search

"It seems like a search component... but it is not."


## Instalação ##
Esse repositório é privado, para cloná-lo você precisa ter a chave pública do ssh cadastrada no bitbucket.

```
npm install git+https://bitbucket.org/colab-sa/not-a-search.git --save
```

Para deploy, será necessário usar uma [deployment key](https://confluence.atlassian.com/bitbucket/use-deployment-keys-294486051.html).


## Uso ##

Ver exemplo no teste: **test-server/src/js/page.js**

Copiar arquivos **.less** da pasta **/src/less** e alterar eles no seu projeto.

```
#!javascript

import NotASearch, {pageType, dateType} from 'not-a-search';
<NotASearch value={value} onChange={this.filterChanged} config={config} />
```


## Compilar (para fazer pull em outros repositórios) ##

1. Incrementar versão no arquivo package.json
1. Executar comando ```npm run compile```
1. Fazer push


## Executar teste ###

```
npm install
npm run test
```

Abrir no browser:
http://localhost:3658/

## Date Range Picker

http://onefinestay.github.io/react-daterange-picker/