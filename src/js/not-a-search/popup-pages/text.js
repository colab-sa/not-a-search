
import React, { Component } from 'react';
import layout from './layout';
import { Field, reduxForm } from 'redux-form';
import PropTypes from 'prop-types';
import _ from 'lodash';
import classNames from 'classnames';

class Text extends Component {

    static propTypes = {
        isEdit: PropTypes.bool,
        title: PropTypes.string,
        onBack: PropTypes.func,
        label: PropTypes.string,
        input: PropTypes.object.isRequired,
        backBtn: PropTypes.object,
        textBtn: PropTypes.object,
        onSubmit: PropTypes.func,
        handleSubmit: PropTypes.func,
        pristine: PropTypes.bool,
        submitting: PropTypes.bool,
        parseResult: PropTypes.func
    }

    constructor(props) {
        super(props);

        const methods = ['submit', 'renderField'];
        _.forEach(methods, method => this[method] = this[method].bind(this));
    }

    submit(form) {
        const { onSubmit, input, parseResult } = this.props;

        if (parseResult) {
            onSubmit({
                [input.name]: parseResult(form[input.name])
            });
            return;
        }

        onSubmit(form);
    }

    renderField(params) {
        const { input, className, type, meta: { touched, error, warning }, ...rest } = params;

        return (
            <input {...input} type={type || 'text'} className={classNames(className, { error: touched && error, warning: touched && warning })} {...rest} />
        );
    }

    render() {

        const { title, onBack, backBtn, isEdit, label, input, textBtn, handleSubmit, pristine, submitting } = this.props;

        const textBtnValue = textBtn[isEdit ? 'isEdit' : 'default'];

        return layout(title, !isEdit && onBack, backBtn, (
            <form className="text-page" onSubmit={handleSubmit(this.submit)}>
                <div>
                    {label ?
                        (<label>{label}</label>)
                        : null
                    }
                    <Field autoFocus {...input} component={this.renderField} />
                </div>
                <div>
                    <button className="submit-btn" type="submit" disabled={pristine || submitting}>{textBtnValue}</button>
                </div>
            </form>
        ));

    }
}



export default key => reduxForm({
    form: `advanced-filter/popup/text/${key}`
})(Text);