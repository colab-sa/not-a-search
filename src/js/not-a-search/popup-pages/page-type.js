'use strict';

export const MAIN = 0;
export const MULTI = 1;
export const RADIO = 2;
export const TEXT = 3;
export const WHEN = 4;
export const MULTITREE = 5;
