'use strict';

import React, { Component } from 'react';
import layout from './layout';
import _ from 'lodash';
import Async from 'react-promise';
import latinize from 'latinize'; 
import PropTypes from 'prop-types';

class MultiTree extends Component {

    constructor(props) {
        super(props);

        this.state = {
            filter: '',
            form: props.initialValues || {
                [props.name]: []
            },
            opened: {}
        };

        this.setFilter = this.setFilter.bind(this);
        this.submit = this.submit.bind(this);
        this.checkboxChange = this.checkboxChange.bind(this);
        this.allSelectedChange = this.allSelectedChange.bind(this);
    }


    submit(event) {
        const { onSubmit } = this.props;
        const { form } = this.state;

        event.preventDefault();

        onSubmit(form);
    }

    setFilter(filter) {
        this.setState({
            filter: filter.target.value
        });
    }

    checkboxChange(event) {
        const { name, parseResult } = this.props;
        const value = parseResult(event.target.value);

        const form = this.state.form || {
            [name]: []
        };

        const values = form[name];

        if (values.indexOf(value) !== -1) {
            this.setState({
                form: {
                    [name]: _.difference(values, [value])
                }
            });

            return;
        }

        this.setState({
            form: {
                [name]: [...values, value].sort()
            }
        });
    }

    getChildrenValue(children) {
        return _.map(children, child => child.value) 
    }

    getChildren(options){
        return options.map(option => this.getChildrenValue(option.children));
    }

    getParsedChildren(children){
        return [].concat.apply([], _.filter(children, i => i.length > 0))
    }

    getRealLength(options){
        const children = this.getChildren(options)
        return options.length + this.getParsedChildren(children).length
    }

    allSelectedChange() {
        const { name, getOptions } = this.props;
        
        const form = this.state.form || {
            [name]: []
        };
        
        getOptions().then(options => {
            const parsedChildren = this.getParsedChildren(this.getChildren(options))
            const realLength = this.getRealLength(options)
            
            if (form[name].length === realLength) {
                this.setState({
                    form: {
                        [name]: []
                    }
                });
                return;
            }

            this.setState({
                form: {
                    [name]: options.map(option => option.value)
                }
            });

        }).catch(err => {
            console.log(err);
        });

    }

    toggleOption = (value) => {
        this.setState(prevState => ({
            opened: {
                [value]: prevState[value] ? !prevState[value] : true
            }
        }))
    }

    render() {

        const { title, onBack, name, backBtn, isEdit, textBtn, getOptions, selectAll, filterFieldPlaceholder, filterIcon } = this.props;
        const { filter, form, opened } = this.state;

        const adjustedFilter = latinize(filter.toLowerCase());
        const textBtnValue = textBtn[isEdit ? 'isEdit' : 'default'];


        return layout(title, !isEdit && onBack, backBtn, (
            <div className="multi-page multiTree-page">

                <div className="filter-wrapper">
                    <input autoFocus className="filter-field" type="text" placeholder={filterFieldPlaceholder} value={filter} onChange={this.setFilter} />
                    {filterIcon}
                </div>

                <form onSubmit={this.submit}>
                    <div className="options-box"> 

                        <Async promise={getOptions()} then={(options) => (
                            <div>
                                {selectAll && !adjustedFilter ? (
                                    <label><input type="checkbox" checked={this.getRealLength(options) === form[name].length} onChange={this.allSelectedChange} /> Todos / Nenhum
                                </label>
                                )
                                    : null
                                }

                                {!adjustedFilter && <hr />}

                                {options.filter(option => latinize(option.label.toLowerCase()).indexOf(adjustedFilter) !== -1).map(option => (
                                    <div className={`multitree__tree ${opened[option.value] ? 'multitree__tree--open' : ''}`}>
                                        <span className={`multitree__arrow ${option.children.length > 0 ? 'multitree__arrow--multiple' : ''}`}
                                            onClick={() => this.toggleOption(option.value)} 
                                        >{opened[option.value] ? <span className="multitree__arrow-icon multitree__arrow-icon--down"></span> : <span className="multitree__arrow-icon multitree__arrow-icon--right"></span> }</span>
                                        {
                                            opened[option.value] ?
                                            <div className="multitree__container">
                                                <label key={option.value} className="multitree__item multitree__item--father">
                                                    <input type="checkbox" name={name} value={option.value} checked={form[name].indexOf(option.value) !== -1} onChange={this.checkboxChange} />
                                                    <p>
                                                        { option.acronym && <span className="multitree__acronym multitree__acronym--father">{option.acronym}</span> }{ option.acronym && ' - ' }{option.label}
                                                    </p>
                                                </label>
                                                {option.children.map(child => 
                                                    <label key={child.value} className="multitree__item multitree__item--children">
                                                        <input type="checkbox" name={name} value={child.value} checked={form[name].indexOf(child.value) !== -1} onChange={this.checkboxChange} />
                                                        <p>
                                                            { child.acronym && <span className="multitree__acronym multitree__acronym--children">{child.acronym}</span> }{ child.acronym && ' - ' }{child.label}
                                                        </p>
                                                    </label>
                                                )}
                                            </div>
                                            :
                                            <label onClick={() => this.toggleOption(option.value)}>                                                  
                                                <p>
                                                    { option.acronym && <span className="multitree__acronym multitree__acronym--father">{option.acronym}</span> }{ option.acronym && ' - ' }{option.label}
                                                </p>
                                            </label>
                                        }
                                    </div>
                                ))}
                            </div>
                        )} pendingRender={(
                            <span>Carregando...</span>
                        )} />

                    </div>
                    <div>
                        <button className="submit-btn" type="submit" disabled={form[name].length === 0}>{textBtnValue}</button>
                    </div>
                </form>

            </div>


        ));

    }
}

MultiTree.propTypes = {
    isEdit: PropTypes.bool,
    title: PropTypes.string,
    onBack: PropTypes.func,
    backBtn: PropTypes.object,
    name: PropTypes.string.isRequired,
    getOptions: PropTypes.func.isRequired,
    textBtn: PropTypes.object,
    parseResult: PropTypes.func,
    onSubmit: PropTypes.func,
    initialValues: PropTypes.object,
    selectAll: PropTypes.string,
    filterFieldPlaceholder: PropTypes.string,
    filterIcon: PropTypes.object
};

export default MultiTree;
