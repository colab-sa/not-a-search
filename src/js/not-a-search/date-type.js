'use strict';

export const today = 'today';
export const yesterday = 'yesterday';
export const last7days = 'last7days';
export const last30days = 'last30days';
export const lastWeek = 'lastWeek';
export const lastMonth = 'lastMonth';
export const lastYear = 'lastYear';
export const thisWeek = 'thisWeek';
export const thisMonth = 'thisMonth';
export const thisYear = 'thisYear';