'use strict';

import React, { Component } from 'react';
import DateRangePicker from '../date-range-picker';
import moment from 'moment';
import classNames from 'classnames';
import InputElement from 'react-input-mask';
import PropTypes from 'prop-types';

const stateDefinitions = {
    available: {
        color: null,
        label: 'Available'
    }
};

const dateRanges = [
];

class DatePicker extends Component {

    constructor(props) {
        super(props);

        const value = typeof props.value === 'object' ? props.value || null : null;

        this.state = {
            bottomValue: value ? {
                start: value.start.format('L'),
                end: value.end.format('L')
            } : {
                    start: '',
                    end: ''
                },
            value: value || null,
            states: null
        };

        this.handleSelect = this.handleSelect.bind(this);
        this.startDateChange = this.startDateChange.bind(this);
        this.endDateChange = this.endDateChange.bind(this);

        this.startDateBlur = this.startDateBlur.bind(this);
        this.endDateBlur = this.endDateBlur.bind(this);

        this.startDateKeyPress = this.startDateKeyPress.bind(this);
        this.endDateKeyPress = this.endDateKeyPress.bind(this);
        this.notify = this.notify.bind(this);
    }

    handleSelect(value, states) {
        this.setState({
            bottomValue: {
                start: value.start.format('L'),
                end: value.end.format('L')
            },
            value,
            states
        }, () => {
            this.notify(value);
        });
    }

    notify(value) {
        const { onChange } = this.props;

        if (onChange) {
            onChange(value);
        }
    }

    startDateChange(event) {

        const { value } = this.state;

        let theValue = value;

        if (typeof theValue !== 'object') {
            theValue = null;
        }

        const isOne = theValue && theValue.end.diff(theValue.start) === 0;

        this.setState({
            bottomValue: {
                start: event.target.value,
                end: isOne ? event.target.value : value ? value.end.format('L') : ''
            }
        });
    }

    startDateKeyPress(event) {
        if (event.key === 'Enter') {
            this.startDateBlur(event);
        }
    }

    endDateChange(event) {
        const { bottomValue } = this.state;

        this.setState({
            bottomValue: {
                start: bottomValue.start,
                end: event.target.value
            }
        });
    }

    endDateKeyPress(event) {
        if (event.key === 'Enter') {
            this.endDateBlur(event);
        }
    }

    startDateBlur(event) {
        //event.preventDefault();

        const { value } = this.state;
        let theValue = value;

        if (typeof theValue !== 'object') {
            theValue = null;
        }

        const isOne = theValue && theValue.end.diff(theValue.start) === 0;
        const newValue = moment(event.target.value, 'L', true);

        if (!newValue.isValid()) {
            this.setState({
                bottomValue: {
                    start: value ? value.start.format('L') : '',
                    end: value ? value.end.format('L') : ''
                }
            });
            return;
        }

        const nextValue = moment.range(newValue, isOne ? newValue : (value ? value.end : ''));

        this.setState({
            bottomValue: {
                start: newValue.format('L'),
                end: isOne ? newValue.format('L') : value ? value.end.format('L') : ''
            },
            value: nextValue
        }, () => {
            this.notify(nextValue);
        });
    }

    endDateBlur(event) {
        //event.preventDefault();

        const { value } = this.state;
        const newValue = moment(event.target.value, 'L', true);

        if (!newValue.isValid()) {
            this.setState({
                bottomValue: {
                    start: value ? value.start.format('L') : '',
                    end: value ? value.end.format('L') : ''
                }
            });
            return;
        }

        const nextValue = moment.range(value ? value.start : '', newValue);

        this.setState({
            bottomValue: {
                start: value ? value.start.format('L') : '',
                end: newValue.format('L')
            },
            value: nextValue
        }, () => {
            this.notify(nextValue);
        });
    }

    getDateMask() {

        const display = moment(new Date(2000, 11, 31)).format('L');

        return {
            mask: display.replace('2000', '9999').replace('12', '19').replace('31', '39'),
            formatChars: {
                '1': '[0-1]',
                '3': '[0-3]',
                '9': '[0-9]'
            }
        };
    }


    renderInputText(value, onChange, onKeyPress, onBlur, autoFocus) {
        return (
            <InputElement {...this.getDateMask() } autoFocus={autoFocus} alwaysShowMask={true} className={classNames({ invalid: value !== '' && !moment(value, 'L', true).isValid() })} type="text" value={value} onChange={onChange} onKeyPress={onKeyPress} onBlur={onBlur} />
        );
    }

    render() {
        const { value, bottomValue } = this.state;

        let theValue = value;

        if (typeof theValue !== 'object') {
            theValue = null;
        }

        const isOne = theValue && theValue.end.diff(theValue.start) === 0;

        return (
            <div className="date-picker">
                <DateRangePicker
                    locale={'pt-br'}
                    firstOfWeek={0}
                    numberOfCalendars={1}
                    selectionType='range'
                    singleDateRange={true}
                    stateDefinitions={stateDefinitions}
                    dateStates={dateRanges}
                    defaultState="available"
                    showLegend={false}
                    value={theValue}
                    onSelect={this.handleSelect}
                />

                {
                    (isOne ?
                        (
                            <div className="display">
                                {this.renderInputText(bottomValue.start, this.startDateChange, this.startDateKeyPress, this.startDateBlur)}
                            </div>
                        ) : (
                            <div className="display">
                                {this.renderInputText(bottomValue.start, this.startDateChange, this.startDateKeyPress, this.startDateBlur, true)}
                                <span> - </span>
                                {this.renderInputText(bottomValue.end, this.endDateChange, this.endDateKeyPress, this.endDateBlur, false)}
                            </div>
                        )
                    )

                }


            </div>
        );
    }

}

DatePicker.propTypes = {
    value: PropTypes.any,
    onChange: PropTypes.func
};

export default DatePicker;