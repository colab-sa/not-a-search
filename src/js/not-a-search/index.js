'use strict';


import React, { Component } from 'react';
import Box from './box';
import Popup from './popup';
import * as _pageType from './popup-pages/page-type';
import * as _dateType from './date-type';
import _ from 'lodash';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import When from './popup-pages/when';

class NotASearch extends Component {

    constructor(props) {
        super(props);

        this.state = {
            value: props.value || {},
            isPopupActive: false
        };

        this.togglePopup = this.togglePopup.bind(this);
        this.addFilter = this.addFilter.bind(this);
        this.removeFilter = this.removeFilter.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            value: nextProps.value
        });
    }

    togglePopup() {
        const { box, popup } = this.refs;
        const { isDisabled } = this.props;

        if (isDisabled) return;

        popup.toggle();
        box.resetSelection();
    }

    addFilter(form) {
        const { onChange, isDisabled } = this.props;
        const { value } = this.state;

        if (isDisabled) return;

        this.setState({
            value: Object.assign({}, value, form)
        }, () => {
            onChange(Object.assign({}, value, form));
        });
    }

    removeFilter(key) {
        const { onChange, isDisabled } = this.props;
        const { value } = this.state;

        if (isDisabled) return;

        this.setState({
            value: _.omit(value, key)
        }, () => {
            onChange(this.state.value);
        });

    }

    render() {

        const { config, className, isDisabled } = this.props;
        const { value, isPopupActive } = this.state;

        return (
            <div className={classNames('not-a-search', className, { 'popup-open': isPopupActive, disabled: isDisabled })}>
                <div className="left">
                    <Popup ref="popup" config={config} onAddFilter={this.addFilter} disableKeys={_.keys(value)} onChangeIsActive={isActive => { this.setState({ isPopupActive: isActive }); }}>
                        <a onClick={this.togglePopup}>+</a>
                    </Popup>
                </div>
                <Box ref="box" config={config} filters={value} onRemoveFilter={this.removeFilter} onAddFilter={this.addFilter} isDisabled={isDisabled} />
            </div>
        );
    }
}

NotASearch.propTypes = {
    value: PropTypes.object,
    onChange: PropTypes.func.isRequired,
    config: PropTypes.object.isRequired,
    className: PropTypes.string,
    isDisabled: PropTypes.bool
};

export default NotASearch;
export const pageType = _pageType;
export const dateType = _dateType;

export const popupPages = {
    When
};