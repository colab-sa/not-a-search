import React, { Component } from 'react';
import PropTypes from 'prop-types';
import shallowEqual from './utils/shallowEqual';
import bemCx from './utils/bemCx';

class PaginationArrow extends Component {

  static propTypes = {
    disabled: PropTypes.bool,
    onTrigger: PropTypes.func,
    direction: PropTypes.oneOf(['next', 'previous']),
    bemNamespace: PropTypes.string,
    bemBlock: PropTypes.string
  };

  static contextTypes = {
    bemNamespace: PropTypes.string,
    bemBlock: PropTypes.string
  };

  static childContextTypes = {
    bemNamespace: PropTypes.string,
    bemBlock: PropTypes.string
  }

  static defaultProps = {
    disabled: false
  };

  constructor(props) {
    super(props);

    this.render = this.render.bind(this);
    this.shouldComponentUpdate = this.shouldComponentUpdate.bind(this);
    this.getChildContext = this.getChildContext.bind(this);
    this.getBemNamespace = this.getBemNamespace.bind(this);
    this.getBemBlock = this.getBemBlock.bind(this);
    this.cx = this.cx.bind(this);
  }

  render() {
    let { disabled, direction, onTrigger, ...props } = this.props;
    let modifiers = { [direction]: true };
    let states = { disabled };

    let elementOpts = {
      modifiers,
      states,
    };

    let iconOpts = {
      element: 'PaginationArrowIcon',
      modifiers,
      states,
    };

    return (
      <div className={this.cx(elementOpts)} {...props} onClick={onTrigger}>
        <div className={this.cx(iconOpts)} />
      </div>
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      !shallowEqual(this.props, nextProps) ||
      !shallowEqual(this.state, nextState)
    );
  }

  getChildContext() {
    return {
      bemNamespace: this.getBemNamespace(),
      bemBlock: this.getBemBlock(),
    };
  }

  getBemNamespace() {
    if (this.props.bemNamespace) {
      return this.props.bemNamespace;
    }
    if (this.context.bemNamespace) {
      return this.context.bemNamespace;
    }
    return null;
  }

  getBemBlock() {
    if (this.props.bemBlock) {
      return this.props.bemBlock;
    }
    if (this.context.bemBlock) {
      return this.context.bemBlock;
    }
    return null;
  }

  cx(options = {}) {
    let opts = {
      namespace: this.getBemNamespace(),
      element: 'PaginationArrow',
      block: this.getBemBlock(),
    };

    Object.assign(opts, options);
    return bemCx(opts);
  }
}

export default PaginationArrow;
