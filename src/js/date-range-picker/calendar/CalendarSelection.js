import React, { Component } from 'react';
import PropTypes from 'prop-types';
import shallowEqual from '../utils/shallowEqual';
import bemCx from '../utils/bemCx';

class CalendarSelection extends Component {

  static propTypes = {
    modifier: PropTypes.string,
    pending: PropTypes.bool.isRequired,
    bemNamespace: PropTypes.string,
    bemBlock: PropTypes.string
  };

  static contextTypes = {
    bemNamespace: PropTypes.string,
    bemBlock: PropTypes.string
  };

  static childContextTypes = {
    bemNamespace: PropTypes.string,
    bemBlock: PropTypes.string
  }

  constructor(props) {
    super(props);

    this.render = this.render.bind(this);
    this.shouldComponentUpdate = this.shouldComponentUpdate.bind(this);
    this.getChildContext = this.getChildContext.bind(this);
    this.getBemNamespace = this.getBemNamespace.bind(this);
    this.getBemBlock = this.getBemBlock.bind(this);
    this.cx = this.cx.bind(this);
  }

  render() {
    let {modifier, pending} = this.props;
    let modifiers = {[modifier]: true};
    let states = {
      pending,
    };

    return (
      <div className={this.cx({states, modifiers})} />
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      !shallowEqual(this.props, nextProps) ||
      !shallowEqual(this.state, nextState)
    );
  }

  getChildContext() {
    return {
      bemNamespace: this.getBemNamespace(),
      bemBlock: this.getBemBlock(),
    };
  }

  getBemNamespace() {
    if (this.props.bemNamespace) {
      return this.props.bemNamespace;
    }
    if (this.context.bemNamespace) {
      return this.context.bemNamespace;
    }
    return null;
  }

  getBemBlock() {
    if (this.props.bemBlock) {
      return this.props.bemBlock;
    }
    if (this.context.bemBlock) {
      return this.context.bemBlock;
    }
    return null;
  }

  cx(options = {}) {
    let opts = {
      namespace: this.getBemNamespace(),
      element: 'CalendarSelection',
      block: this.getBemBlock(),
    };

    Object.assign(opts, options);
    return bemCx(opts);
  }
}

export default CalendarSelection;
