import React, {Component} from 'react';

import PropTypes from 'prop-types';
import shallowEqual from './utils/shallowEqual';
import bemCx from './utils/bemCx';

class Legend extends Component {

  static propTypes =  {
    selectedLabel: PropTypes.string.isRequired,
    stateDefinitions: PropTypes.object.isRequired,
    bemNamespace: PropTypes.string,
    bemBlock: PropTypes.string
  };

  static contextTypes = {
    bemNamespace: PropTypes.string,
    bemBlock: PropTypes.string
  };

  static childContextTypes = {
    bemNamespace: PropTypes.string,
    bemBlock: PropTypes.string
  }

  constructor(props) {
    super(props);

    this.render = this.render.bind(this);
    this.shouldComponentUpdate = this.shouldComponentUpdate.bind(this);
    this.getChildContext = this.getChildContext.bind(this);
    this.getBemNamespace = this.getBemNamespace.bind(this);
    this.getBemBlock = this.getBemBlock.bind(this);
    this.cx = this.cx.bind(this);
  }

  render() {
    let {selectedLabel, stateDefinitions} = this.props;
    let items = [];
    let name;
    let def;
    let style;

    for (name in stateDefinitions) {
      def = stateDefinitions[name];
      if (def.label && def.color) {
        style = {
          backgroundColor: def.color,
        };
        items.push(
          <li className={this.cx({element: 'LegendItem'})} key={name}>
            <span className={this.cx({element: 'LegendItemColor'})} style={style} />
            <span className={this.cx({element: 'LegendItemLabel'})}>{def.label}</span>
          </li>
        );
      }
    }

    return (
      <ul className={this.cx()}>
        <li className={this.cx({element: 'LegendItem'})}>
          <span className={this.cx({element: 'LegendItemColor', modifiers: {'selection': true}})} />
          <span className={this.cx({element: 'LegendItemLabel'})}>{selectedLabel}</span>
        </li>
        {items}
      </ul>
    );
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      !shallowEqual(this.props, nextProps) ||
      !shallowEqual(this.state, nextState)
    );
  }

  getChildContext() {
    return {
      bemNamespace: this.getBemNamespace(),
      bemBlock: this.getBemBlock(),
    };
  }

  getBemNamespace() {
    if (this.props.bemNamespace) {
      return this.props.bemNamespace;
    }
    if (this.context.bemNamespace) {
      return this.context.bemNamespace;
    }
    return null;
  }

  getBemBlock() {
    if (this.props.bemBlock) {
      return this.props.bemBlock;
    }
    if (this.context.bemBlock) {
      return this.context.bemBlock;
    }
    return null;
  }

  cx(options = {}) {
    let opts = {
      namespace: this.getBemNamespace(),
      element: 'Legend',
      block: this.getBemBlock(),
    };

    Object.assign(opts, options);
    return bemCx(opts);
  }
}

export default Legend;
